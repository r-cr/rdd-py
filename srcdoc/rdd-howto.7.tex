\documentclass{article}
\author{Random Crew}
\usepackage[utf8]{inputenc}
\usepackage{latex2man}
\usepackage{devdoc}

\setVersion{1.2}
\begin{document}

\begin{Name}{7}{rdd-howto}{Random Crew}{rdd manuals}{step-by-step introduction to rdd}

    \Prog{rdd-howto}
    – step-by-step introduction to \Cmd{rdd}{1}.

\section{Description}
    This manual describes the essential usage of \Cmd{rdd}{1} from the very beginning.

    \Cmd{rdd}{1} takes user-described libraries, constructs executable script from them and launches. Data used inside the script and list of called functions are given by a user (in command line or config files). Script construction and launch can be repeated several times, if a user wants to launch his code for the series of objects.

\section{Hello, world!}
    Create these two files in current directory:

    \File{hello.sh}
    \begin{verbatim}
main()
{
    printf "%s\n" "Hello, world!"
}
    \end{verbatim}

    \File{rdd.def}
    \begin{verbatim}
[first]

rdd_map_libs = shell hello.sh
    \end{verbatim}

    And launch this:
    \begin{verbatim}
    $ rdd first object main
    \end{verbatim}

    The result you should see:
    \begin{verbatim}
Hello, world!
    \end{verbatim}

    Now we'll see what lies behind all that.

    First to mention is no function calls inside \File{hello.sh}. There is only implementation of \Prog{main}.\\
    Note the absence of shebang \verb+#!/bin/sh+ in \File{hello.sh} as well (and no execution permissions on file).

    The next file \File{rdd.def} contains some data in INI format.\\
    The name \Arg{first} of data section is arbitrary. It contains variables, describing properties of the code we want to use.\\
    The variable \Envar{rdd\_map\_libs} contains files with the used code. First word-specificator tells the file is written in \Prog{shell} language. Our library is assigned without absolute path - in that case path is interpreted as relative to the root of working directory.

    Launch string has the following meaning.
    \begin{verbatim}
    $ rdd first object main
    \end{verbatim}

    \Arg{first} is the name of the data section we chose to use in the launch. Input data for our script code is calculated from here. The argument is the \emph{adjective} of the launch.

    \Arg{object} is the name of some entity we operate on. Objects are needed, when one repeats his actions in cycle for some list, and each entry of the list may define it's custom properties. The argument is the \emph{noun} of the launch.

    \Arg{main} is the name of the shell function we launch. The argument is the \emph{verb} of the launch.

    Each of the three main parameters above can be given explicitly by variable.
    \begin{itemize}
        \item[] \Arg{rdd\_prf\_entry} is the name of data section.
        \item[] \Arg{rdd\_list\_entry} is the name of used objects.
        \item[] \Arg{rdd\_map\_entry} is the name of the launched function.
    \end{itemize}

    Now let's shorten the launch string and save all parameters to config file.\\
    \File{rdd.def} for short command string:
    \begin{verbatim}
[first]

rdd_map_entry = main
rdd_list_entry = object

rdd_map_libs = shell hello.sh
    \end{verbatim}

    With key data, saved inside config, the launch looks like:
    \begin{verbatim}
    $ rdd first
    \end{verbatim}
    Parameter \Arg{first} to initialise \Envar{rdd\_prf\_entry} is still required, as we need to know data section, which contains all other necessary data.\\
    The variant with two arguments is also possible:
    \begin{verbatim}
    $ rdd first object
    \end{verbatim}
    Function name is ommitted with two arguments given.

\section{Hello, friends}
    Each of the three \Cmd{rdd}{1}'s main arguments can contain a list of arguments instead of a single value. Extend \File{hello.sh} functionality a bit to illustrate that.

    \File{hello.sh} with friends:
    \begin{verbatim}
hello_print()
{
    printf "%s\n" "Hello, ${rdd_atom_id}"
}

hello_length()
{
    hello_print | wc -c
}

hello_profiled_print()
{
    printf "%s" "${rdd_prf_entry}: "
    hello_print
}

hello_all()
{
    hello_print
    hello_length
}
    \end{verbatim}

    Launch string with several values:
    \begin{verbatim}
    $ rdd first thing,more_thing hello_print,hello_length
    \end{verbatim}
    And the result:
    \begin{verbatim}
Hello, thing
13
Hello, more_thing
18
    \end{verbatim}
    Here one can see the launch of two functions \Arg{hello\_print} and \Arg{hello\_length}.\\
    Moreover, they are called twice: for the object \Arg{thing} and \Arg{more\_thing}.\\
    The code also demonstrates simple usage of input data, described above, inside functions.

    Let's extend our configuration file also.\\
    \File{rdd.def} with several data sections:
    \begin{verbatim}
[first]

rdd_map_entry = hello_print, hello_length
rdd_list_entry = thing, more_thing

rdd_map_libs = shell hello.sh

[second]

rdd_map_entry = hello_profiled_print, hello_length
rdd_list_entry = thing, more_thing, side_thing

rdd_map_libs = shell hello.sh
    \end{verbatim}

    Launch the example:
    \begin{verbatim}
    $ rdd first
    \end{verbatim}
    And the result, same as the previous one:
    \begin{verbatim}
Hello, thing
13
Hello, more_thing
18
    \end{verbatim}

    Change the name for argument:
    \begin{verbatim}
    $ rdd second
    \end{verbatim}
    The result:
    \begin{verbatim}
second: Hello, thing
13
second: Hello, more_thing
18
second: Hello, side_thing
18
    \end{verbatim}
    All parameters are hidden in config now, and we can manage launches with short command line.

\section{Data combination}
    In the last version of config file the data about language is duplicated.\\
    To get rid off that, we can combine data sections.

    \File{rdd.def} with structured data sections
    \begin{verbatim}
[first]

rdd_map_entry = hello_print, hello_length
rdd_list_entry = thing, more_thing

rdd_prf_id = shell

[second]

rdd_map_entry = hello_profiled_print, hello_length
rdd_list_entry = thing, more_thing, side_thing

rdd_prf_id = shell

[shell]

rdd_map_libs = shell hello.sh
    \end{verbatim}
    The launch and results remain the same:
    \begin{verbatim}
    $ rdd first
    \end{verbatim}

    \begin{verbatim}
Hello, thing
13
Hello, more_thing
18
    \end{verbatim}

    \begin{verbatim}
    $ rdd second
    \end{verbatim}

    \begin{verbatim}
second: Hello, thing
13
second: Hello, more_thing
18
second: Hello, side_thing
18
    \end{verbatim}

    But this time we describe the data only once. Lang variables reside in separate section, and \Arg{first} and \Arg{second} sections include it. To use some data section inside other section, \Envar{rdd\_prf\_id} is used.\\
    The command line equivalent for data inclusion is:
    \begin{verbatim}
    $ rdd first,shell
    \end{verbatim}

    Define more data sections to include several ones at once and see their priorities.

    \File{rdd.def} for chainloading
    \begin{verbatim}
[add_some]

last_data = some
uniq_some_data = qwerty

[add_another]

last_data = another
uniq_another_data = asdfgh

[first]

rdd_map_entry = hello_print, hello_length,add_print
rdd_list_entry = thing, more_thing

rdd_prf_id = shell

[second]

rdd_map_entry = hello_profiled_print, hello_length
rdd_list_entry = thing, more_thing, side_thing

rdd_prf_id = shell

[shell]

rdd_map_libs = shell hello.sh, shell add.sh
    \end{verbatim}

    Note the appearence of the new library file \File{add.sh} and printing method \Arg{add\_print}.

    \File{add.sh}
    \begin{verbatim}
add_print()
{
    printf "%s\n" "${last_data}"
    printf "%s\n" "${uniq_some_data}"
    printf "%s\n" "${uniq_another_data}"
}
    \end{verbatim}
    The launch:
    \begin{verbatim}
    $ rdd first,add_some,add_another
    \end{verbatim}
    The result:
    \begin{verbatim}
Hello, thing
13
another
qwerty
asdfgh
Hello, more_thing
18
another
qwerty
asdfgh
    \end{verbatim}

    The most right data section in the list (as in command line, so inside config file) has the highest priority.\\
    Thus the variable \Envar{last\_data} is taken from section \Arg{add\_another} as the most right one in command line.\\
    At the same time non-overlapping variables \Envar{uniq\_some\_data} and \Envar{uniq\_another\_data} were gathered each from its own section.

    \Envar{rdd\_prf\_id} is not redefined, when mentioned in several sections.\\
    Instead all mentioned sections are summarised and each met section is included.

    The same data can be given in three forms, first has the most priority.\\
    Options in command line:
    \begin{verbatim}
    $ rdd first some_var=value
    \end{verbatim}
    Value in \File{rdd.def} file\\
    Value in \File{*.conf} file inside \File{rdd.conf.d/} dir at the root of working copy

    To list all gathered data, \Cmd{datardd}{1} is used:
    \begin{verbatim}
    $ datardd first
    \end{verbatim}

    \begin{verbatim}
rdd_atom_id=thing
rdd_list_entry=thing, more_thing
rdd_map_libs=shell hello.sh, shell add.sh
rdd_map_entry=hello_print, hello_length,add_print
rdd_prf_all=shell,first
rdd_prf_entry=first
    \end{verbatim}
    The output contains one set of data regardless of quantity of objects in configuration.

\section{Objects and lists}
    Object names can be kept in plain text lists.\\
    \Envar{rdd\_list\_path} variable contains path to directory with such lists.
    \begin{verbatim}
    $ echo 'rdd_list_path = buildfiles' >> rdd.def
    \end{verbatim}
    When lists are set up, they can be used as:
    \begin{verbatim}
    $ rdd first,add_some list.src
    \end{verbatim}
    with example content of \File{buildfiles/list.src}
    \begin{verbatim}
thing

# comments are ingored
# as well as empty strings

more_thing
    \end{verbatim}

    Lists can be nested, thus some list may contain another lists.

    To show resulting list of objects, \Cmd{lsrdd}{1} is used.
    \begin{verbatim}
    $ lsrdd first list.src
    \end{verbatim}
    \begin{verbatim}
thing
more_thing
    \end{verbatim}

    Object may store their custom data.\\
    \Envar{rdd\_atom\_path} variable contains path to directory with atom data.
    \begin{verbatim}
    $ echo 'rdd_atom_path = atomfiles' >> rdd.def
    \end{verbatim}
    Data of objects now is checked up in corresponding file \File{atomfiles/}\Arg{NAME}\File{/atom.conf}.\\
    Priority of data from \File{atom.conf} is between \File{rdd.conf.d/} and \File{rdd.def}.\\
    Used example above will not demonstrate the data from \File{atom.conf}, as it stores all data in \File{rdd.def} with higher priority.

\section{Script dumps}
    All the \Cmd{rdd}{1} actually does is writing down a script with user's wishes and executing it. Have a look at it:
    \begin{verbatim}
    $ rdd first,add_some thing
    \end{verbatim}
    Dumped scripts for all launches reside in \File{var/dump/} directory.\\
    For the launch above scripts lie in \File{var/dump/thing\_first\_add\_some/}.\\
    Note the name of final personal dir: it contains name of the object and data sections, given in command line, \Arg{first} and \Arg{add\_some}. It does not contain section \Arg{shell} in the name, though the section is in use. That's because minimal identification for resulting data in dir name is used.

    First essential file in dump dir is \File{var/dump/thing\_first\_add\_some/dump.sh}.
    It contains all the data and inclusion of libraries, declared for use in configs.\\
    The file does not contain any calls. Its purpose is to make available needed data and functions to the scripts, doing nothing on itself.

    \File{var/dump/thing\_first\_add\_some/dump.sh}
    \begin{verbatim}
export last_data="some"
export rdd_atom_id="thing"
export rdd_list_entry="thing"
export rdd_list_path="buildfiles"
export rdd_map_libs="shell hello.sh, shell add.sh"
export rdd_map_entry="hello_print, hello_length,add_print"
export rdd_prf_all="shell,first,add_some"
export rdd_prf_entry="first,add_some"
export uniq_some_data="qwerty"

. /home/user/poligon/hello.sh
. /home/user/poligon/add.sh
    \end{verbatim}

    Another essential file in dump dir is \File{var/dump/thing\_first\_add\_some/calldump}.\\
    It includes \File{dump.sh} and calls all ordered functions.\\
    This file is executed by \Cmd{rdd}{1}, can be re-executed directly, saved and reused on regular purpose.

    \File{var/dump/thing\_first\_add\_some/calldump}
    \begin{verbatim}
(
#!/bin/sh < /dev/null
. /home/user/poligon/var/dump/thing_first_add_some/dump.sh
hello_print
hello_length
add_print

)
    \end{verbatim}

\section{See also}
    \Cmd{rdd}{1}, the main tool of the set.\\
    \Cmd{datardd}{1}, tool to output data from rdd config files.\\
    \Cmd{dumprdd}{1}, tool to output language script, constructed from rdd config files.\\
    \Cmd{droprdd}{1}, tool to write down script, constructed from rdd config files.\\
    \Cmd{lsrdd}{1}, tool to output list of rdd objects.\\
    \Cmd{rootrdd}{1}, tool to output root of rdd working directory.

%@% IF !embed %@%
\section{Authors}
    Random Crew\\
    \URL{https://opendistro.org/rdd}
%@% END-IF %@%

\end{Name}

\LatexManEnd
\end{document}
