#!/bin/sh

INSTALL=${INSTALL:-"cp -Rf"}
MKDIR=${MKDIR:-"mkdir -p"}
DESTDIR=${DESTDIR:-"${HOME}/rrr"}
PREFIX=${PREFIX:-} 
P=${P:-"rdd"}
BINDIR="${BINDIR:-${PREFIX}/bin}"
DOCDIR="${DOCDIR:-${PREFIX}/share/doc/${P}}"
MANDIR="${MANDIR:-${PREFIX}/share/man}"
LIBDIR="${LIBDIR:-${PREFIX}/lib}"
RCRDIR="${PREFIX}/libexec"
ETCDIR="/etc"
PYTHON=${PYTHON:-"python2"}

if [ -f conf/config.sh ] ; then
    . conf/config.sh
fi

install_bin()
{
    ${MKDIR} "${DESTDIR}${BINDIR}/"
    ${INSTALL} \
        'bin/rdd' \
        'bin/droprdd' \
        'bin/datardd' \
        'bin/dumprdd' \
        'bin/lsrdd' \
        'bin/rootrdd' \
        "${DESTDIR}${BINDIR}/"
    chmod 555 \
        "${DESTDIR}${BINDIR}/rdd" \
        "${DESTDIR}${BINDIR}/droprdd" \
        "${DESTDIR}${BINDIR}/datardd" \
        "${DESTDIR}${BINDIR}/dumprdd" \
        "${DESTDIR}${BINDIR}/rootrdd" \
        "${DESTDIR}${BINDIR}/lsrdd"
}
install_lib()
{
    ${MKDIR} "${DESTDIR}${LIBDIR}/python2.7/site-packages/rdd/"
    ${INSTALL} 'lib/rdd/'*.py "${DESTDIR}${LIBDIR}/python2.7/site-packages/rdd/"
    ${PYTHON} -m 'py_compile' "${DESTDIR}${LIBDIR}/python2.7/site-packages/rdd/"*.py
}
install_doc()
{
    ${MKDIR} "${DESTDIR}${DOCDIR}/"
    ${MKDIR} "${DESTDIR}${MANDIR}/man1/"
    ${MKDIR} "${DESTDIR}${MANDIR}/man7/"

    ${INSTALL} \
        'README-rdd' \
        'License-ISC' \
        doc/html/*.html \
        "${DESTDIR}${DOCDIR}/"

    ${INSTALL} \
        'doc/man/man1/rdd.1' \
        'doc/man/man1/datardd.1' \
        'doc/man/man1/droprdd.1' \
        'doc/man/man1/dumprdd.1' \
        'doc/man/man1/lsrdd.1' \
        'doc/man/man1/rootrdd.1' \
        "${DESTDIR}${MANDIR}/man1/"

    ${INSTALL} \
        'doc/man/man7/rdd-howto.7' \
        "${DESTDIR}${MANDIR}/man7/"
}
install_etc()
{
    ${MKDIR} "${DESTDIR}${ETCDIR}/"
    ${INSTALL} 'etc/.' "${DESTDIR}${ETCDIR}/"
}

install_selftest()
{
    ${MKDIR} "${DESTDIR}${RCRDIR}/"
    ${INSTALL} 'libexec/rddtest' "${DESTDIR}${RCRDIR}/"

    chmod 555 \
        "${DESTDIR}${RCRDIR}/rddtest/atomplace/atomplace" \
        "${DESTDIR}${RCRDIR}/rddtest/atomplace/t" \
        "${DESTDIR}${RCRDIR}/rddtest/atomplace/t_"* \
        "${DESTDIR}${RCRDIR}/rddtest/conf/conf" \
        "${DESTDIR}${RCRDIR}/rddtest/conf/t_"* \
        "${DESTDIR}${RCRDIR}/rddtest/dump/dump" \
        "${DESTDIR}${RCRDIR}/rddtest/dump/t_"* \
        "${DESTDIR}${RCRDIR}/rddtest/list/list" \
        "${DESTDIR}${RCRDIR}/rddtest/list/t_"* \
        "${DESTDIR}${RCRDIR}/rddtest/load/load" \
        "${DESTDIR}${RCRDIR}/rddtest/load/t_"* \
        "${DESTDIR}${RCRDIR}/rddtest/opt/opt" \
        "${DESTDIR}${RCRDIR}/rddtest/opt/t" \
        "${DESTDIR}${RCRDIR}/rddtest/opt/t_"* \
        "${DESTDIR}${RCRDIR}/rddtest/phase/phase" \
        "${DESTDIR}${RCRDIR}/rddtest/phase/t_"* \
        "${DESTDIR}${RCRDIR}/rddtest/section/section" \
        "${DESTDIR}${RCRDIR}/rddtest/section/t_"*

    ${MKDIR} "${DESTDIR}${BINDIR}/"
    ${INSTALL} \
        'bin/rddtest' \
        "${DESTDIR}${BINDIR}/"
    chmod 555 \
        "${DESTDIR}${BINDIR}/rddtest"
}

set -o errexit
set -o xtrace

if [ "$1" = 'selftest' ] ; then
    install_selftest
    exit $?
fi
if [ "$1" = 'sampleconf' ] ; then
    install_etc
    exit $?
fi

install_lib
install_bin
install_doc
