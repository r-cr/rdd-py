#!/bin/sh

. $RCR_ROOT/rddtest/header-env.sh
header_included='yes'

mint_dir=$(pwd)
global_mint_dir=$RCR_ROOT/rddtest/list

launch()
{
    args="${@}"
    echo "${res_actual}"
    ${COVERAGE_RUNNER} run -a $( which lsrdd ) ${args}  > ${res_actual} 2>&1
    diff ${mint_dir}/${res_mint} ${res_actual}
}

launch_fatal()
{
    args="${@}"
    echo "${res_actual}"
    ${COVERAGE_RUNNER} run -a $( which lsrdd ) ${args}  2>&1
}

setup_repo()
{
    export LOCAL_CONFIG=$(pwd)/rdd.conf.d
    export LOCAL_ATOM=$(pwd)/control/atom
    export LOCAL_LIST=$(pwd)/control/list

    rm -rf ${LOCAL_CONFIG} 2> /dev/null
    rm -rf ${LOCAL_ATOM} 2> /dev/null
    rm -rf ${LOCAL_LIST} 2> /dev/null

    mkdir -p ${LOCAL_CONFIG}
    mkdir -p ${LOCAL_ATOM}
    mkdir -p ${LOCAL_LIST}
}

rm .coverage 2>/dev/null
rm case_* 2>/dev/null
rm mint_* 2>/dev/null

rm rdd.def 2>/dev/null
rm -rf control 2>/dev/null

setup_repo
