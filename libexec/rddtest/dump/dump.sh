#!/bin/sh

. $RCR_ROOT/rddtest/header-env.sh
header_included='yes'

mint_dir=$RCR_ROOT/rddtest/dump
mint_result=$RCR_ROOT/rddtest/dump


launch_plaindump()
{
    args="$@"
    echo "${res_actual}"
    ${COVERAGE_RUNNER} run -a $( which dumprdd) $args  > ${res_actual} 2>&1
    sed -e '/rdd_atom_dumpdir/d' ${res_actual} > tmp_${res_actual}
    mv tmp_${res_actual} ${res_actual}
    diff ${mint_result}/${res_mint} ${res_actual}
}

launch_plaindrop()
{
    args="$@"
    echo "${res_actual}"
    ${COVERAGE_RUNNER} run -a $( which droprdd) $args  > ${res_actual} 2>&1
    diff ${mint_result}/${res_mint} ${res_actual}
}

launch_plain()
{
    args="$@"
    echo "${res_actual}"
    ${COVERAGE_RUNNER} run -a $( which rdd) $args  > ${res_actual} 2>&1
    diff ${mint_result}/${res_mint} ${res_actual}
}

launch_warning()
{
    args="$@"
    echo "${res_actual}"
    ${COVERAGE_RUNNER} run -a $( which dumprdd ) $args  > ${res_actual} 2>&1
    grep -v "Warning: " ${res_actual}
}
launch_error()
{
    args="$@"
    echo "${res_actual}"
    ${COVERAGE_RUNNER} run -a $( which dumprdd ) $args  > ${res_actual} 2>&1
    grep -v "Error: " ${res_actual}
}
check_diff()
{
    echo "${res_actual}"
    sed -e '/rdd_atom_dumpdir/d' ${res_actual} > tmp_${res_actual}
    mv tmp_${res_actual} ${res_actual}
    diff ${mint_result}/${res_mint} ${res_actual}
}
prepare_diff()
{
    cp ${mint_result}/${res_mint} .
    export store=${mint_result}
    mint_result=$(pwd)
    sed -e "/^#/ d" ${res_mint} > ${res_mint}'.add'
    mv ${res_mint}'.add' ${res_mint}
}
restore_diff()
{
    mint_result=${store}
    unset store
}

setup_repo()
{
    export LOCAL_CONFIG=$(pwd)/rdd.conf.d
    export LOCAL_LIB=$(pwd)/lib
    export LOCAL_LIST=$(pwd)/control/list
    export LOCAL_ATOM=$(pwd)/control/atom

    rm -rf $LOCAL_CONFIG 2> /dev/null
    rm -rf $LOCAL_LIB 2> /dev/null
    rm -rf $LOCAL_LIST 2> /dev/null
    rm -rf $LOCAL_ATOM 2> /dev/null
    rm -rf *.def 2> /dev/null

    mkdir -p ${LOCAL_CONFIG}
    mkdir -p ${LOCAL_LIB}
    mkdir -p ${LOCAL_LIST}
    mkdir -p ${LOCAL_ATOM}
}

setup_workdir()
{
    rm .coverage 2>/dev/null
    rm case_* 2>/dev/null
    rm mint_* 2>/dev/null
    rm -rf ./var 2>/dev/null

    mint_result=$(pwd)/local_mint
    rm -rf ${mint_result} 2>/dev/null
    mkdir ${mint_result}
    cp -pR ${mint_dir}/mint_* ${mint_result}
    for mintfile in ${mint_result}/mint_* ; do
        sed -e "s%_TOKEN_%$(pwd)%g" $mintfile > $mintfile'.dump'
        mv $mintfile'.dump' $mintfile
    done
}

setup_repo
setup_workdir

cp -pR ${mint_dir}/config_001 ${LOCAL_CONFIG}/repo.conf
cp -pR ${mint_dir}/lib/. ${LOCAL_LIB}/
cp -pR ${mint_dir}/control/list/. ${LOCAL_LIST}/
cp -pR ${mint_dir}/control/atom/. ${LOCAL_ATOM}/

