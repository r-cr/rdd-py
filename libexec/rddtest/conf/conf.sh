#!/bin/sh

. $RCR_ROOT/rddtest/header-env.sh
header_included='yes'

mint_dir=$(pwd)

launch()
{
    echo "${res_actual}"
    RDD_INNER_DEBUG=1 ${COVERAGE_RUNNER} run -a ${CUSTOM_CONF_EXEC} ${@} 2> ${res_actual} > /dev/null
    diff ${mint_dir}/${res_mint} ${res_actual}
}

launch_error()
{
    param=$1
    token="Warning: "
    if [ "$param" = "fatal" ] ; then
        token="Error: "
    fi

    echo "${res_actual}"
    ${COVERAGE_RUNNER} run -a ${CUSTOM_CONF_EXEC} ${@} 2> ${res_actual} > /dev/null
    grep -v "${token}" ${res_actual}
}

setup_local_launcher()
{
    # we need to use local copy of launcher, as we'll manipulate global configs near it
    local dir_here=$(pwd)/local
    export CUSTOM_GLOBAL_ROOT=${dir_here}/libexec
    export CUSTOM_GLOBAL_EXEC=${dir_here}/bin/rddtest
    export CUSTOM_GLOBAL_CONFIG=${dir_here}/etc/rdd

    rm -rf ${dir_here} 2> /dev/null
    mkdir -p ${dir_here}/bin
    mkdir -p ${CUSTOM_GLOBAL_ROOT}/rddtest
    mkdir -p ${CUSTOM_GLOBAL_CONFIG}

    cp -pR $(which rddtest) ${CUSTOM_GLOBAL_EXEC}
    cp -pR $RCR_ROOT/rddtest/conf $CUSTOM_GLOBAL_ROOT/rddtest/
    export RCR_ROOT=$CUSTOM_GLOBAL_ROOT
    export CUSTOM_CONF_EXEC="$( which datardd )"
}

setup_repo()
{

    export LOCAL_CONFIG=$(pwd)/rdd.conf.d

    rm -rf $LOCAL_CONFIG 2> /dev/null
    mkdir -p $LOCAL_CONFIG
    rm -f rdd.def
}

rm .coverage 2>/dev/null
rm case_* 2>/dev/null
rm mint_* 2>/dev/null

setup_local_launcher
setup_repo
