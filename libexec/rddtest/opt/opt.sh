#!/bin/sh

. $RCR_ROOT/rddtest/header-env.sh
header_included='yes'

launch()
{
    args="$@"
    echo "${res_actual}"
    ${COVERAGE_RUNNER} run -a `subcom rddtest opt t ` $args  > ${res_actual} 2>&1
    diff $RCR_ROOT/rddtest/opt/${res_mint} ${res_actual}
}

launch_error()
{
    args="$@"
    echo "${res_actual}"
    ${COVERAGE_RUNNER} run -a `subcom rddtest opt t ` $args  > ${res_actual} 2>&1
    grep -v "Error: " ${res_actual}
}

launch_version()
{
    args="$@"
    echo "${res_actual}"
    ${COVERAGE_RUNNER} run -a `subcom rddtest opt t ` $args  > ${res_actual} 2>&1
    grep -v -e '[0-9][0-9.]*' -e '^current$' ${res_actual}
}

launch_orig()
{
    args="$@"
    echo "${res_actual}"
    ${COVERAGE_RUNNER} run -a `which rdd` $args  > ${res_actual} 2>&1
    diff $RCR_ROOT/rddtest/opt/${res_mint} ${res_actual}
}

prepare_large_list()
{
    sum=0
    string=
    base="arg"
    for sum in $(seq 1 1000 ) ; do
        string="$string ${base}${sum}"
    done
    echo "$string"
}
prepare_large_arg()
{
    sum=0
    string=
    for sum in $(seq 1 51 ) ; do
        string="${string}argargarga"
    done
    echo "$string"
}

rm .coverage 2> /dev/null
