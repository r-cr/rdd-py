#!/bin/sh

# testsuite for phase, langrun, map

. $RCR_ROOT/rddtest/header-env.sh
export header_included='yes'

export mint_dir=$RCR_ROOT/rddtest/phase
export mint_result=$RCR_ROOT/rddtest/phase

launch()
{
    args="$@"
    echo "${res_actual}"
    ${COVERAGE_RUNNER} run -a ${CUSTOM_GLOBAL_EXEC} $args  > ${res_actual} 2>&1
    diff ${mint_result}/${res_mint} ${res_actual}
}

launch_general()
{
    launch $@
}
launch_error()
{
    args="$@"
    echo "${res_actual}"
    ${COVERAGE_RUNNER} run -a ${CUSTOM_GLOBAL_EXEC} $args  > ${res_actual} 2>&1
    grep -v "Error: " ${res_actual}
}

setup_local_launcher()
{
    export CUSTOM_GLOBAL_EXEC=$(which rdd)
    export CUSTOM_GLOBAL_ROOT=${RCR_ROOT}
}

setup_repo()
{
    export LOCAL_CONFIG=$(pwd)/rdd.conf.d
    export LOCAL_LIB=$(pwd)/lib
    export LOCAL_LIST=$(pwd)/control/list
    export LOCAL_ATOM=$(pwd)/control/atom

    rm -rf $LOCAL_CONFIG 2> /dev/null
    rm -rf $LOCAL_LIB 2> /dev/null
    rm -rf $LOCAL_LIST 2> /dev/null
    rm -rf $LOCAL_ATOM 2> /dev/null
    rm -rf *.def 2> /dev/null

    mkdir -p ${LOCAL_CONFIG}
    mkdir -p ${LOCAL_LIB}
    mkdir -p ${LOCAL_LIST}
    mkdir -p ${LOCAL_ATOM}
}

setup_workdir()
{
    rm .coverage 2>/dev/null
    rm case_* 2>/dev/null
    rm mint_* 2>/dev/null
    rm -rf ./var 2>/dev/null
}

setup_local_launcher
setup_repo
setup_workdir
