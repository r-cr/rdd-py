#!/bin/sh

. $RCR_ROOT/rddtest/header-env.sh
header_included='yes'

mint_dir=$RCR_ROOT/rddtest/atomplace

launch()
{
    args="${@}"
    echo "${res_actual}"
    ${COVERAGE_RUNNER} run -a $( subcom rddtest atomplace t ) ${args} > ${res_actual} 2>&1 
    diff ${mint_dir}/${res_mint} ${res_actual}
}

setup_repo()
{

    export LOCAL_CONFIG=$(pwd)/rdd.conf.d
    export LOCAL_ATOM=$(pwd)/control/atom

    rm -rf $LOCAL_CONFIG 2> /dev/null
    mkdir -p $LOCAL_CONFIG
    mkdir -p $LOCAL_ATOM
}

rm .coverage 2>/dev/null
rm case_* 2>/dev/null
rm mint_* 2>/dev/null

rm rdd.def 2>/dev/null
rm -rf control 2>/dev/null

setup_repo
