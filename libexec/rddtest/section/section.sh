#!/bin/sh

# testsuite for section

. $RCR_ROOT/rddtest/header-env.sh
header_included='yes'

mint_dir=$RCR_ROOT/rddtest/section/store
mint_result=$RCR_ROOT/rddtest/section

launch()
{
    args="$@"
    echo "${res_actual}"
    : > ${res_actual}
    for atom in $( lsrdd $args 2> /dev/null ) ; do
        ${COVERAGE_RUNNER} run -a $( which datardd ) $args rdd_list_entry=${atom} | grep -e 'rdd_prf_all' >> ${res_actual} 2>&1
    done
    diff ${mint_result}/${res_mint} ${res_actual}
}

launch_full()
{
    args="$@"
    echo "${res_actual}"
    : > ${res_actual}
    ${COVERAGE_RUNNER} run -a $( which rdd ) $args >> ${res_actual} 2>&1
    diff ${mint_result}/${res_mint} ${res_actual}
}

launch_diag()
{
    args="$@"
    echo "${res_actual}"
    ${COVERAGE_RUNNER} run -a $( which datardd ) $args  > ${res_actual} 2>&1
    grep -v "${token}" ${res_actual}
}

launch_error()
{
    export token="Warning: "
    args="${@}"
    launch_diag $args

    unset token
}
launch_fatal()
{
    export token="Error: "
    args="${@}"
    launch_diag $args

    unset token
}

setup_local_launcher()
{
    export CUSTOM_GLOBAL_EXEC=$(which rddtest)
    export CUSTOM_GLOBAL_ROOT=${RCR_ROOT}
}

setup_repo()
{
    export LOCAL_CONFIG=$(pwd)/rdd.conf.d
    export LOCAL_LIB=$(pwd)/lib
    export LOCAL_LIST=$(pwd)/control/list
    export LOCAL_ATOM=$(pwd)/control/atom

    rm -rf $LOCAL_CONFIG 2> /dev/null
    rm -rf $LOCAL_LIB 2> /dev/null
    rm -rf $LOCAL_LIST 2> /dev/null
    rm -rf $LOCAL_ATOM 2> /dev/null
    rm -rf *.def 2> /dev/null

    mkdir -p ${LOCAL_CONFIG}
    mkdir -p ${LOCAL_LIB}
    mkdir -p ${LOCAL_LIST}
    mkdir -p ${LOCAL_ATOM}
}

rm .coverage 2>/dev/null
rm case_* 2>/dev/null
rm mint_* 2>/dev/null

setup_local_launcher
setup_repo
