#!/usr/bin/env python2
#encoding=utf-8

# Parser of specs from configs: keywords with special meaning
# Class doesn't make assumptions on further use of specs, so returns dict
# with everything found, even if something will be useless by caller's logic

class GenericSpecParser(object):

    defSpecsSeparator = ' '
    defSpecsSeparatorAlter = '::'
    defSpecInline = 'inline'

    @staticmethod
    def specParse(entry):
        stumps = GenericSpecParser.getSpecStumps(entry)

        targetSpec = {}
        # target itself is always last in a string
        targetSpec['target'] = stumps.pop()
        targetSpec['inline'] = False
        targetSpec['lang'] = False
        targetSpec['rest'] = []
        for pot in stumps:
            pot = pot.strip()
            if not pot:
                continue
            if pot == GenericSpecParser.defSpecInline:
                targetSpec['inline'] = True
                continue
            if not targetSpec['lang']:
                targetSpec['lang'] = pot
                continue
            targetSpec['rest'].append(pot)
        return targetSpec

    @staticmethod
    def getSpecStumps(entry):
        further = entry.replace(GenericSpecParser.defSpecsSeparatorAlter, GenericSpecParser.defSpecsSeparator)
        stumps = further.strip().split(GenericSpecParser.defSpecsSeparator)
        return stumps

 # ---------------------------------------------------------

# Copyright (c) 2014-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------
           
