#!/usr/bin/env python2
#encoding=utf-8

# director of configs and paths, related to single atom

from os.path import join, isdir, isfile
from os import access, X_OK, R_OK

from lib_rdd_proc import resolveBaseDir, resolveSingleCategoryPath
from lib_rdd_varint import *

class AtomPlaceDescriptor(object):

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # static consts
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # file with personal config
    defConfigAtomFile = 'atom.conf'
    # var with a path to atom data
    optAtomPath = VarInterface.var_atom_path
    # setting whether correct atom requires separate dir 
    optAtomPersonal = VarInterface.var_tune_atom_personal

    def __init__(self, name):
        # object's text name
        self.name = name
        # object with data repo
        self.repo = None
        # dir with personal stuff
        self.atomdir = ''
        # actual pretender for personal config
        self.candidAtomConfig = ''
        # checked configs
        self.atomConfig = []
        # actual option to read the path to atoms
        self.atomPathConfig = ''
        # if atom is done
        self.isAtomDone = False

        self.initAtomNames()

    def initAtomNames(self):
        self.candidAtomConfig = AtomPlaceDescriptor.defConfigAtomFile
        self.atomPathConfig = AtomPlaceDescriptor.optAtomPath

    def loadRepo(self, repo):
        self.repo = repo
        self.isAtomDone = False

        self.loadPersonalPath()

        return self.isAtomGood()

    def loadPersonalPath(self):
        isPersonalNeeded = self.repo.readBoolValue( AtomPlaceDescriptor.optAtomPersonal )
        if not isPersonalNeeded and self.name :
            self.isAtomDone = True

        p = resolveSingleCategoryPath(self.repo, self.atomPathConfig)
        isDir = self.resolvePersonalDir(p)
        if not isDir:
            return

        self.loadConfig()

    def resolvePersonalDir(self, path):
        if not path or not isdir(path):
            return False
        checkDir = join(path, self.name)
        if isdir(checkDir) and access(checkDir, X_OK):
            self.atomdir = checkDir
            self.isAtomDone = True
            return True
        return False

    def loadConfig(self):
        cand = join(self.atomdir, self.candidAtomConfig)
        if isfile(cand) and access(cand, R_OK):
            self.atomConfig.append(cand)

    def isAtomGood(self):
        return self.isAtomDone

    def getConfig(self):
        return self.atomConfig

    def getId(self):
        """ Global atom's ident
        """
        return self.name

    def getDir(self):
        return self.atomdir

# ---------------------------------------------------------

# Copyright (c) 2014-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------
