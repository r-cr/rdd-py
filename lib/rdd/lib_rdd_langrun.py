#!/usr/bin/env python2
#encoding=utf-8

# launch interpreted code and save everything involved in Python space

from subprocess import Popen, PIPE, call
from lib_rdd_proc import timeToPanic

class LangRunner(object):

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(LangRunner, cls).__new__(cls)
        return cls.instance
    
    def __init__(self):
        self.code = ''
        self.execShell = None
        
        # data from subprocess with code
        self.retcode = None
        self.out = ''
        self.errout = ''

    def loadInterpretator(self, interpretator):
        if interpretator:
            self.execShell = interpretator

    def runCodeIntercept(self, code):
        self.code = code

        if self.code:

            proc = Popen(self.code, shell = True, executable=self.execShell, stdout = PIPE, stderr = PIPE, bufsize = 1)
            self.out, self.errout = proc.communicate()
            self.retcode = proc.returncode

            if self.retcode != 0:
                timeToPanic([self.errout])
            
            for stOut in self.out.split('\n'):
                yield stOut

    def runCode(self, code):
        self.code = code

        if self.code:
            call(self.code, shell = True, executable=self.execShell )

    def getReturnCode(self):
        return self.retcode

    def getErrOut(self):
        return self.errout

    def getOut(self):
        return self.out

 # ---------------------------------------------------------

# Copyright (c) 2014-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------
       
