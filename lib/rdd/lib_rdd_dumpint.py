#!/usr/bin/env python2
#encoding=utf-8

# interfaces for dump names with lang resources

from os.path import join

class DumpInterface(object):

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # static consts
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # const prefix in names of interface files with lang resources
    defDumpIntFileName = 'dump'
    # separator between words in file names
    defFileNameSeparator = '_'

    # top dir for any writing
    defDirWriteRoot = 'var'
    # dir for dumps
    defDirDumpRoot = 'dump'

    # methods to get relative paths to dumps 
    # all config (target) data is in the dumps, so we need to know about 
    # dumps' location without configs 
    
    # interfaces define just final tail of directory structure, 
    # allowing to keep dumps in different ways 
    # absolute path before postfix is defined by caller

    @staticmethod
    def getDirDumpCur():
        """ Interface dir for dumps
        """
        return join(DumpInterface.defDirWriteRoot, DumpInterface.defDirDumpRoot)

    @staticmethod
    def getNameForIntDump(langid):
        """
            Construct name for dump
            langid - string, describing lang of the dump
        """
        construct = DumpInterface.defDumpIntFileName
        if langid:
            construct = construct + langid
        return construct

    @staticmethod
    def getBasePlaceForAtom(descList):
        """ Dislocation of atom's dumps relative to parent paths
        """
        finalDesc = ''
        for desc in descList:
            if not finalDesc:
                finalDesc = desc
            else:
                finalDesc = finalDesc + \
                    DumpInterface.defFileNameSeparator + \
                    desc
        return finalDesc

    @staticmethod
    def getCurPlaceForAtom(descList):
        """ Relative path to atom's materials
        """
        return join ( DumpInterface.getDirDumpCur() , DumpInterface.getBasePlaceForAtom(descList) )

    @staticmethod
    def getCurAllForIntDump(descList, langid):
        return join ( DumpInterface.getDirDumpCur() , DumpInterface.getBasePlaceForAtom( descList ) ,
            DumpInterface.getNameForIntDump(langid) )

    @staticmethod
    def getTargetDumpName(dump, targetDesc):
        """ Returns dump name, showing it's specific aim
        """
        return targetDesc + dump


# ---------------------------------------------------------

# Copyright (c) 2013-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------

