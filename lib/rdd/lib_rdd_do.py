#!/usr/bin/env python2
#encoding=utf-8

# coordination between atoms, theirs data and phases to launch

from lib_rdd_print import timeToPanic
from lib_rdd_load import *
from lib_rdd_map import *
from lib_rdd_phase import *
from lib_rdd_list import *
from lib_rdd_dump import *
from lib_rdd_optint import *
from lib_rdd_atomconstructor import *

# -------------------------------------------------

class Steersman(object):

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # static consts
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # name of target object in data storage
    optAtomId = VarInterface.var_atom_id
    # name for global repo
    defRepoId = 'repo'

    def __init__(self):
        # launcher of target phases
        self.phaser = None
        # generator of functions from maps
        self.mapContainer = None
        # generator of atoms
        self.listContainer = None

        # for current single atom
        self.atomDescriptor = None
        # container from lib_load
        self.loader = None
        # keeper with actual data for current single atom
        self.atomKeeper = None
        # to write file dumps
        self.dumpGenerator = None
        # target data of central 'repo' atom
        self.repoData = []

    def spawnRepo(self):
        self.loader = Loader()
        self.repoData = self.loader.getLoadedTargetData()
        self.atomKeeper = AtomConstructor().construct(self.repoData, Steersman.defRepoId)

    def spawnExecutor(self):
        self.phaser = PhaseMaster()
        self.mapContainer = MapMaster()

    def processGroupMap(self):

        self.mapContainer.loadRepoData(self.atomKeeper)
        self.mapContainer.switchToGroupMap()
        self.processMap()

    def processMap(self):
        self.phaser.loadKeeper(self.atomKeeper)
        
        funcName = self.mapContainer.getNextMapAction()
        while funcName:
            self.phaser.processMapEntry(funcName)
            funcName = self.mapContainer.getNextMapAction()
        self.phaser.commitQueuedPhases()

    def processList(self):

        self.spawnAll()
        
        enteredAlready = False

        for atom in self.listContainer.getNextConfigAtomDesc():

            self.atomDescriptor = atom
        
            # all atom data is loaded, no more need in heavy lib_load 
            self.spawnAtom()
            self.loadAtomDropDump()
        
            # the work with atom map has begun

            if not enteredAlready:
                enteredAlready = True
                self.processGroupMap()

            self.processSingleMap()

    def spawnAll(self):

        self.spawnRepo()
        self.spawnDumpers()
        self.spawnExecutor()
        self.spawnList()

    def spawnList(self):
        try:
            self.listContainer = ListMaster()
            self.listContainer.loadRepoData(self.atomKeeper)
        
        except Exception as e:
            text = [
                    'Getting object from list generator failed:',
                    str(e),
                    'No further processing possible.'
                   ]
            timeToPanic(text)

    def spawnAtom(self):

        personalConfigz = self.atomDescriptor.getConfig()
        if personalConfigz:
            atomData = self.loader.getPersonalTargetData(personalConfigz)
        else:
            atomData = self.repoData
        self.atomKeeper = AtomConstructor().construct(atomData, self.atomDescriptor.getId())

    def processSingleMap(self):

        self.mapContainer.loadRepoData(self.atomKeeper)
        self.mapContainer.switchToSingleMap()
        self.processMap()

    def commitGroupMap(self):

        self.spawnRepo()
        self.spawnDumpers()
        self.spawnExecutor()

        self.loadAtomDropDump()
        
        self.processGroupMap()

    def spawnDumpers(self):
        self.dumpGenerator = DumpGenerator()

    def loadAtomDropDump(self):
        self.dumpGenerator.loadRepoData(self.atomKeeper)
        self.dumpGenerator.generateAllDump()

    def dropData(self):
        self.spawnAll()
        repoKeeper = self.atomKeeper

        atom = None
        entered = False
        iterList = self.listContainer.getNextConfigAtomDesc()
        try:
            atom = iterList.next()
        except Exception as e:
            pass

        if atom:
            self.atomDescriptor = atom
            self.spawnAtom()
            self.atomKeeper.printData()
            entered = True

        if not entered:
            repoKeeper.printData()

    def askConf(self):
        self.spawnAll()
        for atom in self.listContainer.getNextConfigAtomDesc():
            targetTrace(atom.getConfig())

    def prepareToAskDump(self):
        self.spawnAll()

        # print for single atom only
        atom = None
        iterList = self.listContainer.getNextConfigAtomDesc()
        try:
            atom = iterList.next()
        except Exception as e:
            pass

        if atom:
            self.atomDescriptor = atom
            self.spawnAtom()

        self.dumpGenerator.loadRepoData(self.atomKeeper)

    def askDump(self):
        self.prepareToAskDump()
        self.dumpGenerator.printInlineAllDump()

    def dropDump(self):
        self.prepareToAskDump()
        self.dumpGenerator.generateAllDump()

    def printDumpName(self):
        OutModes.targetOutput = 1
        self.dumpGenerator.printFormalSumDumps()
        OutModes.targetOutput = 0

# ---------------------------------------------------------

# Copyright (c) 2013-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------

