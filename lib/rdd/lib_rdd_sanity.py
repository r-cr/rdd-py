#!/usr/bin/env python2
#encoding=utf-8

# methods to sanitize input from the wild

from string import maketrans, ascii_letters, digits

# 89909 on Stackoverflow.com, thanks Brian

def areCharsEssential(rawString, moreAllowedChars=''):

    tableForChecking = maketrans('','')
    saneChars=ascii_letters + digits + '_' + moreAllowedChars 

    isValid = not rawString.translate(tableForChecking, saneChars)
    return isValid
