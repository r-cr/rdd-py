#!/usr/bin/env python2
#encoding=utf-8

# static names and symbols for cmd line parsing

class CmdLineOptsInterface(object):

    separatorInOptVars = '='

    # what opts in repo config should be replaced by first cmd params
    shortParamForDo     = 'rdd_map_entry'
    shortParamForAs     = 'rdd_prf_entry'
    shortParamForObj = 'rdd_list_entry'
