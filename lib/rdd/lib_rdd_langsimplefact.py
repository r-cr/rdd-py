#!/usr/bin/env python2
#encoding=utf-8

# generator of object-lang-handler
# makes relation between lang name in config and class name, which handles it

from lib_rdd_print import timeToPanic

class SimpleLangConstructorBreed(object):

    @staticmethod
    def unleashSimpleLangObject(lang):

        if lang == 'shell':
            from lib_rdd_langrel_shell import SimpleLangConstructorShell
            res = SimpleLangConstructorShell()
        elif lang == 'python':
            from lib_rdd_langrel_python import SimpleLangConstructorPython
            res = SimpleLangConstructorPython()
        else:
            timeToPanic( [ 'Lang \'%s\' is unsupported.' % lang ])

        return res

# ---------------------------------------------------------

# Copyright (c) 2014-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------

