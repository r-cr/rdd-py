#!/usr/bin/env python2
#encoding=utf-8

# interface for minimal language support, allowing map usage in this lang

from abc import ABCMeta, abstractmethod, abstractproperty


# ---------------------------------------------------------

# replace for python3
# class SimpleLangConstructor(metaclass=ABCMeta):

class SimpleLangConstructor:
    __metaclass__ = ABCMeta

    stringFailHandle = " || \\"

    @abstractmethod
    def langStringInitVar(varName, varVal):
        """ lang string, setting up variable
        """
        pass

    @abstractmethod
    def langStringCallMeth(methName, *argz):
        """ lang string, calling method
        """
        pass

    @abstractmethod
    def langStringUseLib(libName):
        """ lang string, including library file
        """
        pass

    @abstractmethod
    def langStringMethExist(methName, moduleName):
        """ lang string, printing method name, if one is defined in current namespace
            empty string otherwise
        """
        pass

    @abstractmethod
    def langTextGroupMethExist(methList, moduleName):
        """ lang text, printing methods from given list, 
            which are defined in current namespace
        """
        pass

    @abstractmethod
    def langFilePostfix():
        """ extenstion of file, written in language
        """
        pass

    @abstractmethod
    def embeddable():
        pass

    @abstractmethod
    def langStringBeginCode(interp):
        pass

    @abstractmethod
    def langStringCloseCode():
        pass

# ---------------------------------------------------------

# Copyright (c) 2014-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------

