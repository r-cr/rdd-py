#!/usr/bin/env python2
#encoding=utf-8

# set of target specific classes for dropping and parsing dumps

from lib_rdd_dumpint import *
from lib_rdd_varint import *
from lib_rdd_dumpparser import *

# ---------------------------------------------------------

# hierarchy of targets in dumps

class TargetKeeper(object):

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # static consts
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # var with filters to define all other vars, which should be exported
    optEnvFilter = VarInterface.var_env_filter
    # var with library functions, used in actual lang
    optMapLibs = VarInterface.var_map_libs
    # path to data, personal for atom
    optAtomPath = VarInterface.var_atom_path
    # chars for specificators of language for lib
    allowedLibChars = ': '

    def __init__(self):

        # object of repo
        self.repo = None
        # masks to filter out keys in value dict and export result to environment
        self.maskPrefixez = []

    def initAll(self, keeper):
        self.repo = keeper
        self.initKeyMasksForEnv() 
        self.initEntryParser()

    def initKeyMasksForEnv(self):
        for mask in self.repo.readEssentialStringVectValue(TargetKeeper.optEnvFilter):
            self.maskPrefixez.append(mask)

    def initEntryParser(self):
        self.targetEntryParser = TargetEntryLibParser()

        libDirs = []
        baseDir = resolveBaseDir()
        forAtom = self.repo.readValue(TargetKeeper.optAtomPath)
        personalAtom = self.repo.readValue(VarInterface.var_atom_id)

        libDirs.append( join(baseDir, forAtom, personalAtom) )
        libDirs.append( baseDir )

        self.targetEntryParser.libDirs = libDirs

    def getStuffEnvForGeneration(self):

        varzToDump = self.repo.treasureVarz
        localKeys = varzToDump.keys()
        localKeys.sort()

        for var in localKeys:
            if not self.maskPrefixez:
                yield var
            else:
                for mask in self.maskPrefixez:
                    if var.find(mask) >= 0: 
                        yield var

    def getStuffLibForGeneration(self, lang):

        libz = self.getLibSource()
        for (lib,parsedLang) in self.targetEntryParser.parseRawSource(libz):
            if not ( lang == parsedLang ):
                continue
            for stuff in self.targetEntryParser.textGenCurrent.getSomethingFromLib(lib):
                yield stuff

    def getLibSource(self):
        for src in self.repo.readPathStringVectValue(TargetKeeper.optMapLibs, \
                morechars=TargetKeeper.allowedLibChars):
            yield src

    def getTextEnvToDump(self, langExternalKeeper, varToDump): 
        text = langExternalKeeper.langStringInitVar(varToDump, self.repo.readValue(varToDump) )
        return text

    def getTextLibToDump(self, langExternalKeeper, libToDump): 
        text = self.targetEntryParser.textGenCurrent.getFinalTextFromLib(langExternalKeeper, libToDump)
        return text

    def getTextToDump(self, lang, langExternalKeeper):

        wholeText = ''
        for lineStuff in self.getStuffEnvForGeneration():
            wholeText = wholeText + self.getTextEnvToDump(langExternalKeeper, lineStuff)
        for lineStuff in self.getStuffLibForGeneration(lang):
            wholeText = wholeText + self.getTextLibToDump(langExternalKeeper, lineStuff)

        return wholeText

    def getLangsOfLibs(self):

        libs = self.getLibSource()
        usedLangs = self.targetEntryParser.getUsedLangs(libs)
        return usedLangs

# ---------------------------------------------------------

# Copyright (c) 2013-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------

