#!/usr/bin/env python2
#encoding=utf-8

# phase analysis and launch

from lib_rdd_rawfiles import openRewriteFile, makeDir, writeToFile
from lib_rdd_print import *

from lib_rdd_dumpint import DumpInterface
from lib_rdd_dump import *
from lib_rdd_specint import *
from lib_rdd_langrun import LangRunner
from lib_rdd_optint import *
from lib_rdd_varint import *

from os.path import join, isfile
from os import close

class PhaseMaster(object):

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # static consts
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # opt-setting to control bufferisation of phases
    optTunePhaseVector = VarInterface.var_tune_phase_buffer

    # descriptor for dump with calls
    defCallDump = 'call'
    # descriptor for dump with phase seeking
    defPhaseSeekDump = 'phaseseek'

    # name of shell script in languages
    # it is outer language of call dump
    defOuterLang = 'shell'

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(PhaseMaster, cls).__new__(cls)
        return cls.instance

    def __init__(self):
        self.loadKeeper(None)

    def loadKeeper(self, keeper):
        # data of atom
        self.keeper = keeper

        # objects-wrappers for a lang
        self.langzGlue = {}
        # objects-wrappers of a phase
        self.phasezGlue = {}
        self.phaseCallQueue = []
        # actual langs with real dump
        self.langs = set()
        # whether we should buffer phases
        # (real default value will be loaded in method below)
        self.isPhaseBuffer = False

        self.currentLang = ''
        self.tmpDumpIntParams = {}
        # current text with lang for the definition of methods' existence
        self.curTextSeek = ''
        # current map entries without found method
        self.undefEntries = set()
        # map entries with found method in current iteration
        self.foundPhases = set()
        # inner temporary set 
        self.notFoundInIteration = set()
        # inner descriptor for dump with code
        self.fdCallDump = None

        if not self.keeper:
            return

        # object to construct full dump paths 
        self.dumpNamer = DumpGenerator()
        self.dumpNamer.loadRepoData(self.keeper)
        self.dumpNamer.setLangToDump()
        if not self.addLangToUse(PhaseMaster.defOuterLang):
            timeToPanic(['Failed to support language for main outer launch'])
        for lang in self.dumpNamer.getNextLang():
            self.addLangToUse(lang)

        self.settingPhaseBuffer()

        # object for launcher of target code
        self.codeRunner = LangRunner()
        self.codeRunner.loadInterpretator(self.langzGlue[PhaseMaster.defOuterLang].interp)

    def addLangToUse(self, lang):
        if lang in self.langs:
            return True
        langer = self.dumpNamer.getLanger(lang)
        if not langer:
            return False
        self.langzGlue[lang] = langer
        self.langs.add(lang)
        if not self.currentLang:
            self.currentLang = lang

        return True

    def settingPhaseBuffer(self):
        self.isPhaseBuffer = True
        offPhaseBuffer = self.keeper.readBoolValue( PhaseMaster.optTunePhaseVector )
        if offPhaseBuffer:
            self.isPhaseBuffer = False

    def processMapEntry(self, entry):
        """ Launch action processing
            Returns True, if further handling will take place in PhaseMaster
            Returns False, if nothing will be done

            According to 'phase buffer' setting will whether put the action to queue, 
            which should be launched explicitly with commitQueuedPhases, 
            or will launch an action at once
        """
        self.addToCallQueue(entry)

        if not self.isPhaseBuffer:
            self.commitQueuedPhases()

        return True

    def addToCallQueue(self, entry):
        specStuff = GenericSpecParser.specParse(entry)
        phase = specStuff['target']
        if specStuff['lang']:
            lang = specStuff['lang']
            self.saveBindedPhase(phase, lang)
        else:
            self.undefEntries.add( phase )

        self.phaseCallQueue.append( phase )
        
    def saveBindedPhase(self, phase, lang):
        self.phasezGlue[phase] = lang

    def commitQueuedPhases(self):

        if not self.phaseCallQueue:
            return
        for lang in self.langs:
            if not self.undefEntries:
                break
            self.bindPhasesToLang(lang)
            self.undefEntries = self.notFoundInIteration

        self.commitCall()
        self.cleanupCall()

# -----------------------------------

    def bindPhasesToLang(self, lang):

        self.beginLangIteration(lang)
        self.buildPhaseSeek()
        self.commitPhaseSeek()

    def beginLangIteration(self, lang):
        self.currentLang = lang
        self.notFoundInIteration = set()
        self.foundPhases = set()

    def buildPhaseSeek(self):

        dump = self.langzGlue[self.currentLang].dump
        langKeeper = self.langzGlue[self.currentLang].builder
        interp = self.langzGlue[self.currentLang].interp

        self.curTextSeek = langKeeper.langStringBeginCode(interp)

        # here we rely on the being of a dump in a single file only
        self.curTextSeek = self.curTextSeek + langKeeper.langStringUseLib(dump)

        for entry in langKeeper.langTextGroupMethExist(self.undefEntries, dump):
            self.curTextSeek = self.curTextSeek + entry
        self.curTextSeek = self.curTextSeek + langKeeper.langStringCloseCode()

    def commitPhaseSeek(self):
        self.dumpCall = ''

        fileDescToWrite = self.openDumpFile(PhaseMaster.defPhaseSeekDump)
        code = self.codeLogOpenDescriptor()
        self.logCallCode(code, fileDescToWrite)
        code = self.curTextSeek
        self.logCallCode(code, fileDescToWrite)
        close(fileDescToWrite)

        for found in self.codeRunner.runCodeIntercept(self.dumpCall):
            if found in self.undefEntries:
                self.foundPhases.add(found)
                # we cannot concatenate script for final launch here at once, as they should be called
                # in order from phaseCallQueue, and not by lang grouping
                self.saveBindedPhase(found, self.currentLang)

        self.notFoundInIteration = self.undefEntries.difference(self.foundPhases)

# -----------------------------------

    def commitCall(self):
        storeLang = ''
        self.dumpCall = ''

        fileDescToWrite = self.openDumpFile(PhaseMaster.defCallDump)
        code = self.codeLogEntireTarget()
        self.logCallCode(code, fileDescToWrite)
            
        for phase in self.phaseCallQueue:
            if not phase in self.phasezGlue:
                warningTrace([ "phase \'%s\' was not called" % phase ])
                continue

            lang = self.phasezGlue[phase]
            if not lang in self.langzGlue:
                warningTrace([ "phase \'%s %s\' was not called" % (lang, phase) ])
                continue

            code = self.codeCallMeth(lang, phase)

            if not lang == storeLang:
                code = self.codeNewSubShell(lang) + code
                if storeLang:
                    code = self.codeClosePrevious(storeLang) + code
                storeLang = lang

            self.logCallCode(code, fileDescToWrite)

        if storeLang:
            code = self.codeClosePrevious(storeLang)
            self.logCallCode(code, fileDescToWrite)
        close(fileDescToWrite)

        self.codeRunner.runCode(self.dumpCall)

    def codeCallMeth(self, lang, meth):
        interp = self.langzGlue[lang].interp
        dump = self.langzGlue[lang].dump
        code = self.langzGlue[lang].builder.langStringCallMeth(meth, dump, interp)

        return code

    def codeNewSubShell(self, lang):

        dump = self.langzGlue[lang].dump
        langKeeper = self.langzGlue[lang].builder
        interp = self.langzGlue[lang].interp

        text = langKeeper.langStringBeginCode(interp)
        text = text + langKeeper.langStringUseLib(dump)

        return text

    def codeClosePrevious(self, lang):
        langKeeper = self.langzGlue[lang].builder
        handlingCode = ''
        continueForClose = False
        for handler in self.keeper.readEssentialStringVectValue(VarInterface.var_map_postmortem):
            handlingCode = handlingCode + self.codeCallMeth(lang, handler)

        if handlingCode:
            continueForClose = True
            handlingCode = self.codeErrorHandlingOpener(lang) + \
                    handlingCode + \
                    self.codeErrorHandlingCloser(lang) + \
                    langKeeper.langStringCloseCode()

        code = langKeeper.langStringCloseCode(continueForClose)
        code = code + handlingCode

        return code

    def codeErrorHandlingOpener(self, lang):

        if lang == PhaseMaster.defOuterLang:
            dump = self.langzGlue[lang].dump
            langKeeper = self.langzGlue[lang].builder
            interp = self.langzGlue[lang].interp

            code = langKeeper.langTextErrorHandleBegin(interp)
            code = code + langKeeper.langStringUseLib(dump)
        else:
            code = self.codeNewSubShell(lang)

        return code

    def codeErrorHandlingCloser(self, lang):
        code = ''
        if lang == PhaseMaster.defOuterLang:
            langKeeper = self.langzGlue[lang].builder
            code = langKeeper.shellTextCatchStatusClose
        return code

    def logCallCode(self, code, fd):
        self.dumpCall = self.dumpCall + code
        writeToFile(fd, code)

    def openDumpFile(self, dumpname):

        tryDir = self.dumpNamer.getRootDumpDir()
        makeDir(tryDir)
        baseFileName = DumpInterface.getTargetDumpName( DumpInterface.getNameForIntDump(''), dumpname)
        finalFileName = join (tryDir, baseFileName)
        fdCallDump = openRewriteFile(finalFileName)
        return fdCallDump

    def cleanupCall(self):
        self.undefEntries = set()
        self.phaseCallQueue = []

    def codeLogEntireTarget(self):
        logcode=''
        stdoutRedirect = self.keeper.readBoolValue(VarInterface.var_tune_stdout_redirect)
        stderrRedirect = self.keeper.readBoolValue(VarInterface.var_tune_stderr_redirect)

        if stdoutRedirect or stderrRedirect:

            shellKeeper = self.langzGlue[PhaseMaster.defOuterLang].builder
            descriptorTerminal = self.keeper.readValue(VarInterface.var_log_num_terminal)
            logcode = shellKeeper.langStringNewStream(descriptorTerminal)

            logfile = self.keeper.readValue(VarInterface.var_log_stdout)
            if stdoutRedirect:
                logcode = logcode + shellKeeper.langStringOutRedirect(logfile)
            if stderrRedirect:
                if stdoutRedirect:
                    logcode = logcode + shellKeeper.langStringErrDublicate()
                else:
                    logcode = logcode + shellKeeper.langStringErrRedirect(logfile)

        return logcode

    def codeLogOpenDescriptor(self):
        logcode=''
        stdoutRedirect = self.keeper.readBoolValue(VarInterface.var_tune_stdout_redirect)
        stderrRedirect = self.keeper.readBoolValue(VarInterface.var_tune_stderr_redirect)

        if stdoutRedirect or stderrRedirect:

            shellKeeper = self.langzGlue[PhaseMaster.defOuterLang].builder
            descriptorTerminal = self.keeper.readValue(VarInterface.var_log_num_terminal)
            logfile = self.keeper.readValue(VarInterface.var_log_stdout)
            logcode = shellKeeper.langStringNewStreamToFile(descriptorTerminal, logfile)

        return logcode

# ---------------------------------------------------------

# Copyright (c) 2013-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------

# NOTE
#   if there's phase in the dump, queue it
#   if no, commit current queue, empty it and execute directly this phase
# commit queue at the end
