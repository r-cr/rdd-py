#!/usr/bin/env python2
#encoding=utf-8

# low-level file methods

from os import open, O_WRONLY, O_RDONLY, O_TRUNC, O_CREAT, O_APPEND, unlink, listdir, makedirs, write
from os.path import exists, isdir
from shutil import rmtree

from lib_rdd_print import timeToPanic

def openRewriteFile(fileName):
    try:
        fd = open(fileName, O_WRONLY | O_TRUNC | O_CREAT )
    except Exception as e:
        text = [
                'Cannot open file for rewriting:',
                str(e),
                'Emergency exit.'
               ]
        timeToPanic(text)
    return fd

def openAppendFile(fileName):
    try:
        fd = open(fileName, O_WRONLY | O_CREAT | O_APPEND )
    except Exception as e:
        text = [
                'Cannot open file for appending:',
                str(e),
                'Emergency exit.'
               ]
        timeToPanic(text)
    return fd

def openReadFile(fileName):
    try:
        fd = open(fileName, O_RDONLY )
    except Exception as e:
        text = [
                'Cannot open file for reading:',
                str(e),
                'Emergency exit.'
               ]
        timeToPanic(text)
    return fd

def removeFile(fileName):
    try:
        if exists(fileName):
            unlink (fileName)
    except Exception as e:
        text = [
                'Cannot remove file:',
                str(e),
                'Emergency exit.'
               ]
        timeToPanic(text)

def removeDir(dirName):
    try:
        if isdir(dirName):
            rmtree(dirName)
    except Exception as e:
        text = [ 
                'Cannot remove dir:', 
                str(e), 
                'Emergency exit.' 
               ]
        timeToPanic(text)

def lsFromDir(dirName):
    try:
        if isdir(dirName):
            return listdir(dirName)
    except Exception as e:
        text = [ 
                'Cannot list dir:', 
                str(e), 
                'Emergency exit.' 
               ]
        timeToPanic(text)
    return None

def makeDir(dirName):
    try:
        if not isdir(dirName):
            makedirs(dirName)
    except Exception as e:
        text = [ 
                'Cannot make dirs:', 
                str(e), 
                'Emergency exit.' 
               ]
        timeToPanic(text)
        
def writeToFile(fileDesc, textToWrite):
    try:
        write(fileDesc, textToWrite)
    except Exception as e:
        text = [ 
                'Cannot write to file:', 
                str(e), 
                'Emergency exit.' 
               ]
        timeToPanic(text)

# ---------------------------------------------------------

# Copyright (c) 2014-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------

