#!/usr/bin/env python2
#encoding=utf-8

# minimal support for maps in posix shell

from lib_rdd_langsimple import *

# ---------------------------------------------------------

class SimpleLangConstructorShell(SimpleLangConstructor):

    shellext = '.sh'
    newL = '\n'
    shellStringInitVar = '{0}="{1}"'
    shellStringCallMeth = "{0}"
    shellStringUseLib = ". '{0}'"
    shellTextEmbedClose = """
) """
    shellTextEmbedOpenOnly = """
(
"""
    shellStringInterpretator = "#!{0} < /dev/null"
    shellTextMethodPrint = """

existing_methods()
{{
    while [ ! -z "${{1}}" ] ; do
        meth_to_check="${{1}}"
        meth_output="$(type ${{meth_to_check}})"
        meth_code=$?
        if [ ${{meth_code}} = '0' ] && [ ! -z "${{meth_output}}" ] ; then
            echo ${{meth_to_check}}
        fi
        shift
    done
}}

existing_methods {0}
"""

    shellTextCatchStatusOpen = """
rdd_exit_status=$?
export rdd_exit_status
if [ "${rdd_exit_status}" != 0 ] ; then
"""
    shellTextCatchStatusClose = """
fi
"""

    shellStringNewStream = " exec {0}<&1"
    shellStringNewStreamToFile = " exec {0}> {1}"
    shellStringOutRedirect = " : > {0} ; exec 1> {0} "
    shellStringErrRedirect = " : > {0} ; exec 2> {0} "
    shellStringErrDublicate = " exec 2>&1 "

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(SimpleLangConstructorShell, cls).__new__(cls)
        return cls.instance

    def __init__(self):
        pass

    def langStringInitVar(self, varName, varVal):
        text = SimpleLangConstructorShell.shellStringInitVar.format(varName, varVal) \
            + SimpleLangConstructorShell.newL
        return text

    def langStringCallMeth(self, methName, *argz):
        text = SimpleLangConstructorShell.shellStringCallMeth.format(methName) \
            + SimpleLangConstructorShell.newL
        return text

    def langStringUseLib(self, libName):
        text = SimpleLangConstructorShell.shellStringUseLib.format(libName) \
            + SimpleLangConstructorShell.newL
        return text

    def langStringMethExist(self, methName, moduleName ):
        text = SimpleLangConstructorShell.shellTextMethodPrint.format(methName)
        return text

    def langTextGroupMethExist(self, methList, moduleName ):
        stringMeth = ''
        for meth in methList:
            stringMeth = stringMeth + ' ' + meth
        text = SimpleLangConstructorShell.shellTextMethodPrint.format(stringMeth)
        return text

    def langFilePostfix(self):
        return SimpleLangConstructorShell.shellext

    def embeddable(self):
        return True

    def langStringBeginCode(self, interp):
        code = SimpleLangConstructorShell.shellTextEmbedOpenOnly
        code = code + SimpleLangConstructorShell.shellStringInterpretator.format(interp) \
                + SimpleLangConstructorShell.newL
        return code

    def langStringCloseCode(self, tohandle=False):
        code = SimpleLangConstructorShell.shellTextEmbedClose
        # tohandle is ignored on purpose
        return code

    def langStringNewStream(self, numStream ):
        return SimpleLangConstructorShell.shellStringNewStream.format(numStream) \
                + SimpleLangConstructorShell.newL

    def langStringNewStreamToFile(self, numStream, filename ):
        return SimpleLangConstructorShell.shellStringNewStreamToFile.format(numStream, filename) \
                + SimpleLangConstructorShell.newL

    def langStringOutRedirect(self, placeToRedirect ):
        return SimpleLangConstructorShell.shellStringOutRedirect.format(placeToRedirect) \
                + SimpleLangConstructorShell.newL

    def langStringErrRedirect(self, placeToRedirect ):
        return SimpleLangConstructorShell.shellStringErrRedirect.format(placeToRedirect) \
                + SimpleLangConstructorShell.newL

    def langStringErrDublicate(self):
        return SimpleLangConstructorShell.shellStringErrDublicate \
                + SimpleLangConstructorShell.newL

    def langTextErrorHandleBegin(self, interp):
        code = SimpleLangConstructorShell.shellTextEmbedOpenOnly
        code = code + SimpleLangConstructorShell.shellTextCatchStatusOpen
        code = code + SimpleLangConstructorShell.shellStringInterpretator.format(interp)
        code = code + SimpleLangConstructorShell.newL

        return code

# ---------------------------------------------------------

# Copyright (c) 2015-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------

