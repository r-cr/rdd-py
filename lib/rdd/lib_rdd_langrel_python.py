#!/usr/bin/env python2
#encoding=utf-8

# minimal support for maps in Python

from lib_rdd_langsimple import *
from os.path import basename, dirname

# ---------------------------------------------------------

class SimpleLangConstructorPython(SimpleLangConstructor):
    
    pyext = '.py'
    newL = '\n'
    pyStringInitVar = "{0} = '{1}'"
    pyStringCallMeth = "{0}( {1} )"
    pyStringUseSeparateLib = "import {0}"
    pyStringUseLib = "from {0} import *"
    pyStringUseSystem = "from sys import path"
    pyStringMethCallable = """
if callable(getattr({0}, '{1}', None) ):
    print ( '{1}' )
"""
    pyStringAddSearchPath = """
if not '{0}' in path:
    path.insert(0, '{0}' )
"""
    pyStringEmbedOpen = """
(
{0} << TEH_END"""
    pyStringEmbedClose = """TEH_END
) """

    def __init__(self):
        pass

    def langStringInitVar(self, varName, varVal):
        return SimpleLangConstructorPython.pyStringInitVar.format(varName, varVal) \
            + SimpleLangConstructorPython.newL

    def langStringCallMeth(self, methName, *argz):
        return SimpleLangConstructorPython.pyStringCallMeth.format(methName, '') \
            + SimpleLangConstructorPython.newL

    def langStringUseSeparateLib(self, libName):
        text, modName = self.pythonPrepareImport(libName)
        text = text + SimpleLangConstructorPython.pyStringUseSeparateLib.format(modName) \
            + SimpleLangConstructorPython.newL

        return text

    def pythonPrepareImport(self, libName):
        initPyText = SimpleLangConstructorPython.pyStringUseSystem \
            + SimpleLangConstructorPython.newL
        path = dirname(libName)
        text = initPyText + SimpleLangConstructorPython.pyStringAddSearchPath.format(path)
        modName = basename(libName)
        modName = self.pythonGetGoodModuleName(modName)
        return text, modName

    def pythonGetGoodModuleName(self, rawName):
        return rawName.replace(SimpleLangConstructorPython.pyext, '', 1)

    def langStringUseLib(self, libName):
        text, modName = self.pythonPrepareImport(libName)
        text = text + SimpleLangConstructorPython.pyStringUseLib.format(modName) \
            + SimpleLangConstructorPython.newL

        return text

    def langStringMethExist(self, methName, moduleName):

        text = self.langStringUseSeparateLib(moduleName)
        text = text + self.pythonTextMethCallable(methName, moduleName)

        return text

    def pythonTextMethCallable(self, methName, moduleName):
        modName = self.pythonGetGoodModuleName( basename(moduleName) )
        return SimpleLangConstructorPython.pyStringMethCallable.format(modName, methName)

    def langTextGroupMethExist(self, methList, moduleName):

        text = self.langStringUseSeparateLib(moduleName)
        for meth in methList:
	        text = text + self.pythonTextMethCallable(meth, moduleName)

        return text

    def langFilePostfix(self):
        return SimpleLangConstructorPython.pyext

    def embeddable(self):
        return True

    def langStringBeginCode(self, interp):
        return SimpleLangConstructorPython.pyStringEmbedOpen.format(interp) \
            + SimpleLangConstructorPython.newL

    def langStringCloseCode(self, tohandle=False):
        code = SimpleLangConstructorPython.pyStringEmbedClose
        if tohandle:
            code = code + SimpleLangConstructor.stringFailHandle

        return code

# ---------------------------------------------------------

# Copyright (c) 2014-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------

