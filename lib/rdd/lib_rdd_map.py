#!/usr/bin/env python2
#encoding=utf-8

# handle maps from repo ; generate function list from map

from lib_rdd_print import warningTrace, generalTrace
from lib_rdd_varint import *

# ---------------------------------------------------------

class MapMaster(object):

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # static consts
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # add permitted chars to generic essential set
    allowedMapChars = ': '

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(MapMaster, cls).__new__(cls)
        return cls.instance

    def __init__(self):
        # prepared list of funcs to give out by generator
        self.readyQueue = []
        # object with repo data
        self.repo = None

    def clearQueue(self):
        self.readyQueue = []
        
    def loadRepoData(self, repoExtern):
        self.repo = repoExtern

    def switchToGroupMap(self):
        self.clearQueue()
        self.loadStringIntoMap(VarInterface.var_map_group)
        if not self.readyQueue:
            generalTrace([ 'No text for group map was found (within map_group option).' ])

    def loadStringIntoMap(self, key):
        for strCand in self.repo.readEssentialStringVectValue(key, morechars=MapMaster.allowedMapChars):
            self.readyQueue.append(strCand)

    def switchToSingleMap(self):
        self.clearQueue()
        self.loadStringIntoMap(VarInterface.var_map_autopre)
        self.loadStringIntoMap(VarInterface.var_map_entry)
        self.loadStringIntoMap(VarInterface.var_map_autopost)
        if not self.readyQueue:
            warningTrace([ 'No text for map was found (within map_entry, map_autopre, map_autopost options).' ])

    def getNextMapAction(self):
        if self.readyQueue:
            gen = self.readyQueue.pop(0)
            return gen
        return None

# ---------------------------------------------------------

# Copyright (c) 2013-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------
