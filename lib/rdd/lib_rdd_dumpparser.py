#!/usr/bin/env python2
#encoding=utf-8

# set of classes to help dump for TargetKeeperLib

from lib_rdd_specint import *
from lib_rdd_proc import *

from os.path import isabs, join, isfile

# ---------------------------------------------------------

class TargetEntryLibParser(object):

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(TargetEntryLibParser, cls).__new__(cls)
        return cls.instance
    
    def __init__(self):
        self.lang = ''
        self.entry = ''
        # list of dirs to search for needed files
        self.libDirs = []

        # handlers of lang text generation
        self.textGenDumperLib = TargetEntryLibDumper()
        self.textGenDumperLibInline = TargetEntryLibDumperInline()
        self.textGenCurrent = self.textGenDumperLib

    def soInit(self, rawSource=''):
        self.lang = ''
        self.entry = ''
        self.textGenCurrent = self.textGenDumperLib

        self.specStuff = GenericSpecParser.specParse(rawSource)

    def parseRawSource(self, rawSource):

        for src in rawSource:
            self.soInit(src)
            if self.processGoodLib():
                yield ( self.entry, self.lang )

    def processGoodLib(self):
        if not self.setGoodFile(self.specStuff['target']):
            return False
        if self.specStuff['inline']:
            self.textGenCurrent = self.textGenDumperLibInline
        if self.specStuff['lang']:
            self.lang = self.specStuff['lang']

        if not self.lang:
            warningTrace([ "Could not detect the lang for lib entry: %s" % self.entry , "Lib is ignored." ])
            return False

        return True

    def setGoodFile(self, stump):
        if isabs(stump):
            self.entry = stump
            return True
        else:
            for dirCheck in self.libDirs:
                pretender = join(dirCheck, stump)
                if isfile(pretender):
                    self.entry = pretender
                    return True
        return False

    def getUsedLangs(self, rawLibz):
        langs = set()

        for lib in rawLibz:
            self.soInit(lib)
            if self.processGoodLib():
                langs.add(self.lang)

        return langs

class TargetEntryLibDumper(object):

    def __init__(self):
        pass
    
    # for simple including of libs nothing is needed 
    def getSomethingFromLib(self, lib): 
        yield lib

    def getFinalTextFromLib(self, langExternalKeeper, libToDump): 
        return langExternalKeeper.langStringUseLib(libToDump)

class TargetEntryLibDumperInline(TargetEntryLibDumper):

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # static consts
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # maximum code string length allowed for inline 
    defInlStringLength = 500
    # to separate inline libs
    defInlineLibTail = "\n" 

    def getSomethingFromLib(self, lib): 
        for line in parseFile(lib):
            yield line
        yield TargetEntryLibDumperInline.defInlineLibTail

    # direct content writing, no lang construction
    def getFinalTextFromLib(self, langExternalKeeper, string): 
        return string

# ---------------------------------------------------------

# Copyright (c) 2013-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------

