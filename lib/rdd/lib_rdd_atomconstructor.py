#!/usr/bin/env python2
#encoding=utf-8

# create atomkeeper from raw data and descriptors of any kinds

from lib_rdd_varint import *
from lib_rdd_atom import *
from lib_rdd_dump import DumpGenerator

from os.path import join

# -------------------------------------------------

class AtomConstructor(object):

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # static consts
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # name of target object in data storage
    optAtomId = VarInterface.var_atom_id

    # name of var with stream for terminal output
    optStreamTerminal = VarInterface.var_log_num_terminal

    # var with log file for output redirect
    optStdoutLog = VarInterface.var_log_stdout

    # var to keep dump directory of atom
    optAtomDir = VarInterface.var_atom_dumpdir

    # default num of stream to keep access to standart output after 
    # stdout redirection inside target script
    defStdOutSaved = 3

    # name of log for redirected standart output of target script
    defLog = 'log'

    def __init__(self):
        pass

    def construct(self, data, descriptor):

        self.preCreateKeeper(data, descriptor)
        atomKeeper = AtomKeeper( data )
        self.postCreateKeeper(atomKeeper)

        return atomKeeper

    def preCreateKeeper(self, data, descriptor):

        data[AtomConstructor.optAtomId] = descriptor

    def postCreateKeeper(self, keeper):

        atomDir = self.getAtomDir(keeper)
        keeper.saveValue(AtomConstructor.optAtomDir, atomDir)

        stdoutRedirect = keeper.readBoolValue(VarInterface.var_tune_stdout_redirect)
        stderrRedirect = keeper.readBoolValue(VarInterface.var_tune_stderr_redirect)

        if stdoutRedirect or stderrRedirect:

            rawNum = keeper.readValue(AtomConstructor.optStreamTerminal)
            goodNum = self.getTerminalDescriptor(rawNum)
            keeper.saveValue(AtomConstructor.optStreamTerminal, goodNum)

            logFile = join(atomDir, AtomConstructor.defLog)
            keeper.saveValue(AtomConstructor.optStdoutLog, logFile)

    def getAtomDir(self, keeper):
        dumpNamer = DumpGenerator()
        dumpNamer.loadRepoData(keeper)
        logDir = dumpNamer.getRootDumpDir()
        return logDir

    def getTerminalDescriptor(self, rawDesc):
        descriptorTerminal = rawDesc
        if descriptorTerminal:
            descriptorTerminal = int(descriptorTerminal[0])
        if not descriptorTerminal or descriptorTerminal < 3 or descriptorTerminal > 9:
            descriptorTerminal = AtomConstructor.defStdOutSaved 
        descriptorTerminal = str(descriptorTerminal)
        return descriptorTerminal

# ---------------------------------------------------------

# Copyright (c) 2015-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------
