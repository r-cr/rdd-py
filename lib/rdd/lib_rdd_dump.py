#!/usr/bin/env python2
#encoding=utf-8

# saving dumps to disk with target lang elements (variables, functions)
# dumps contain full environment for lang namespace to execute target functions from map

from os import close
from os.path import join, split

from lib_rdd_proc import *
from lib_rdd_rawfiles import *
from lib_rdd_print import *
from lib_rdd_dumpint import *
from lib_rdd_dumptarget import *

from lib_rdd_langsimplefact import *
from lib_rdd_varint import *
from lib_rdd_sanity import *

# ---------------------------------------------------------

class DumpGenerator(object):

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # static consts
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # to separate logical pieces
    defFileTail = "\n"
    # explicit separator
    defFileSeparator = "#------------\n"
    # constants to define shell language name and it's default interpretator
    # (as it differs from default scheme of other languages)
    defShellIdent = 'shell'
    defShellBuiltinInterpretator = 'sh'

    # prefix to mark explicitly the interpreter for the lang
    interpPrefix = VarInterface.prefix_var_lang
    # opt to assign used langs
    optMapLang = VarInterface.var_map_lang

    def __init__(self):
        # repository object
        self.repo = None

        # dict of keepers for lang-related in dump
        self.dumpKeepers = {}
        # keeper of specific of dump
        self.targetKeeper = None
        # whether we got some text for lang in current iteration
        self.langEntered = False

        # root dir before dump directories
        self.dirname = ''
        # strings, describing dump name
        self.descNames = []

# --------~~~~~~~~~--------
# interfaces

    def loadRepoData(self, repoExtern):
        del self.repo
        self.repo = repoExtern
        del self.dumpKeepers
        self.dumpKeepers = {}

        self.dirname = resolveBaseDir()
        atomName = self.repo.readValue(VarInterface.var_atom_id)
        self.descNames = [ atomName ]
        for desc in self.repo.readEssentialStringVectValue(VarInterface.var_prf_entry):
            self.descNames.append(desc)

    def generateAllDump(self):
        removeDir(self.getRootDumpDir())

        self.setLangToDump()
        self.openDumpFiles()
        self.commitDump()
        self.closeDescriptors()

    def printFormalSumDumps(self):
        dumpText = self.getFormalSumDumps()
        targetTrace(dumpText)

    def printInlineAllDump(self):
        self.setLangToDump()
        for lang in self.getLangsToDump():
            self.printInlineDump(lang)
            self.printLangSeparator()

# --------~~~~~~~~~--------

    def setLangToDump(self):

        self.targetKeeper = TargetKeeper()
        self.targetKeeper.initAll(self.repo)

        for lang in self.getNextLang():
            if not lang in self.dumpKeepers or not self.dumpKeepers[lang]:
                langerForDump = self.getLanger(lang)
                if langerForDump:
                    self.dumpKeepers[lang] = langerForDump 
                else:
                    continue

    def getLangsToDump(self):
        for keeper in self.dumpKeepers:
            yield keeper

    def openDumpFiles(self):
        for lang in self.dumpKeepers:
            self.dumpKeepers[lang].openDescriptor()

    def commitDump(self):

        usedLangs = set()
        for lang in self.getLangsToDump():

            textToDump = self.targetKeeper.getTextToDump(lang, self.dumpKeepers[lang].getKeeper())
            fileToWrite = self.dumpKeepers[lang].getDescriptor()
            writeToFile( fileToWrite, textToDump )
            if textToDump:
                usedLangs.add(lang)

        textToDump = DumpGenerator.defFileTail
        for lang in usedLangs:
            fileToWrite = self.dumpKeepers[lang].getDescriptor()
            writeToFile( fileToWrite, textToDump )

    def closeDescriptors(self):
        for lang in self.dumpKeepers:
            self.dumpKeepers[lang].closeDescriptor()

    def getFormalSumDumps(self):
        for lang in self.dumpKeepers:
            yield self.dumpKeepers[lang].getDump()
        
    def printInlineDump(self, lang):
        textToPrint = self.targetKeeper.getTextToDump(lang, self.dumpKeepers[lang].getKeeper())
        for stringToDump in textToPrint:
            targetStringTrace(stringToDump)
            self.langEntered = True
        if self.langEntered:
            targetStringTrace(DumpGenerator.defFileTail)

    def printLangSeparator(self):
        if self.langEntered:
            targetStringTrace(DumpGenerator.defFileSeparator)
        self.langEntered = False

# ---------------------------------------------------------

    def getLanger(self, lang):

        langer = AboutLang(lang)

        langPostfix = langer.builder.langFilePostfix()
        langer.dump = join ( self.dirname, 
            DumpInterface.getCurAllForIntDump(self.descNames, langPostfix) )
        langer.interp = self.getLangInterpretator(lang)

        if not langer.interp:
            warningTrace([ 'Interpretator for lang \'%s\' was not found' % lang ])
            return False
        return langer

    def getLangInterpretator(self, lang):
        keyInterp = DumpGenerator.interpPrefix + lang
        valInterp = self.repo.readValue(keyInterp)
        if not valInterp:
            valInterp = lang
            if lang == DumpGenerator.defShellIdent:
                valInterp = DumpGenerator.defShellBuiltinInterpretator

        return whichExec(valInterp)

    def getNextLang(self):

        usedLangs = set()
        for lang in self.repo.readEssentialStringVectValue( DumpGenerator.optMapLang ):
            usedLangs.add(lang)
        if not usedLangs:
            usedLangs = self.targetKeeper.getLangsOfLibs()

        for lang in usedLangs:
            yield lang

    def getRootDumpDir(self):
        return join(self.dirname, DumpInterface.getCurPlaceForAtom(self.descNames) )

# ---------------------------------------------------------

# there is limited quantity of langs, we can afford homemade struct
# instead of dict
class AboutLang(object):

    def __init__(self, lang):
        self.lang = lang
        self.dump = ''
        self.builder = SimpleLangConstructorBreed.unleashSimpleLangObject(lang)
        self.interp = ''
        self.filedescriptor = None

    def getKeeper(self):
        return self.builder

    def getDump(self):
        return self.dump

    def getDescriptor(self):
        return self.filedescriptor

    def openDescriptor(self):
        trydir, tryfile = split(self.dump)
        makeDir(trydir)

        self.filedescriptor = openAppendFile(self.dump)

    def closeDescriptor(self):
        if self.filedescriptor:
            close (self.filedescriptor)
        self.filedescriptor = None


# ---------------------------------------------------------

# Copyright (c) 2013-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------

