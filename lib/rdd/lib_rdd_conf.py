#!/usr/bin/env python2
#encoding=utf-8

# reading configs into Python namespace

# : Outer data

from lib_rdd_print import *
from lib_rdd_rawfiles import lsFromDir
from lib_rdd_proc import resolveBaseDir
from lib_rdd_opt import OuterWorldInput

from os import access, R_OK, environ, putenv
from os.path import abspath, join, isdir, isfile, dirname

class ConfigMaster(object):

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # static consts
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    defGlobalConfigPathz = [ '../etc/rdd', '../../etc/rdd' ]
    defLocalConfigPathz = [ 'rdd.conf.d' ]
    defLocalConfigMarker = 'rdd.conf.d'
    # embedded sources of def values
    defDefineFilez = [ 'rdd.def' ]
    # section with vars to begin with 
    defDefineSectionMain = 'default'

    innerCycleLimit = 200
    configFilesLimit = 4096

    varLocalRoot = 'RDD_ROOT'

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(ConfigMaster, cls).__new__(cls)
        return cls.instance

    def __init__(self):

        # paths to search for library configs
        self.searchGlobalConfigDirz = []
        # paths to search for local library configs
        self.searchLocalConfigDirz = []

        # sorted files of library configs
        self.confFilez = []

        # list of define files to find and check
        self.noncheckedDefFilez = []
        # found and checked def files
        self.defFilez = []
        # root directory for repo
        self.repoRoot = ''

    def readAllConfigs(self):

        self.initStartConfigs()
        self.resolveLocalRoot()
        self.loadRoot()
        self.getConfigFilez()
        self.prepareConfigFilez()

    def getDynamicConfigFilez(self):
        return self.confFilez

    def getStaticConfigFilez(self):
        return self.defFilez

    def initStartConfigs(self):
        """ Place to add more initial config files, if would be needed
        """
        self.searchGlobalConfigDirz = ConfigMaster.defGlobalConfigPathz
        self.searchLocalConfigDirz = ConfigMaster.defLocalConfigPathz

        self.repoRoot = resolveBaseDir()
        self.noncheckedDefFilez = ConfigMaster.defDefineFilez

    def resolveLocalRoot(self):

        curRootDir = self.repoRoot
        boolToContinue = True
        sanityCounter = 0

        while boolToContinue:

            for defFile in self.noncheckedDefFilez:
                curRepoFile = abspath(join(curRootDir, defFile))
                if access(curRepoFile, R_OK):
                    boolToContinue = False
                    self.repoRoot = curRootDir
                    self.defFilez.append(curRepoFile)
                    return

            curRepoDir = abspath(join(curRootDir, ConfigMaster.defLocalConfigMarker))
            if access(curRepoDir, R_OK):
                boolToContinue = False
                self.repoRoot = curRootDir
                return

            curRepoFile = dirname(curRootDir)
            if curRepoFile != curRootDir and access(curRepoFile, R_OK):
                curRootDir = curRepoFile
            else:
                boolToContinue = False

            sanityCounter = sanityCounter + 1
            if sanityCounter > ConfigMaster.innerCycleLimit:
                boolToContinue = False

        if sanityCounter > ConfigMaster.innerCycleLimit:
            timeToPanic(["""
Limit of %d on nested directories has been reached, while searching for configs of working copy.
That means most probably broken or looped filesystem.
Or you have really long directory tree.
""" % ConfigMaster.innerCycleLimit ])

        warningTrace(["""
Files with configs haven't been found.
Using current directory as the root of working copy.
"""])

    def loadRoot(self):
        environ[ConfigMaster.varLocalRoot] = self.repoRoot
        putenv(ConfigMaster.varLocalRoot, self.repoRoot)

        #-----------------------------

    def getConfigFilez(self):
        """ creating list of checked config files from all search paths
        """ 
        configDir = self.resolveConfigPath()

        if configDir:
            limitCounter = 0
            for wannabeConfig in lsFromDir(configDir):

                if limitCounter >= ConfigMaster.configFilesLimit:
                    timeToPanic(["""
Limit of %d on maximum files inside configuration directory has been reached.
Check the correctness of rdd.conf.d/ directory.
This limit applies on every file inside rdd.conf.d, not just on correct *.conf.
In case of real usage that amount of files, join them into lesser quantity.
""" % ConfigMaster.configFilesLimit ])
                limitCounter = limitCounter + 1

                if not wannabeConfig[0].isalpha():
                    continue
                absWannabeConfig = join(configDir, wannabeConfig)
                if isfile(absWannabeConfig):
                    self.confFilez.append(absWannabeConfig)

    def resolveConfigPath(self):
        """ creating list of dirs where to search for configs
        """
        targetDir = ''

        rootCandidate = self.repoRoot
        if rootCandidate:

            for moreDir in self.searchLocalConfigDirz:
                elem = abspath(join(rootCandidate, moreDir) )
                if isdir (elem):
                    targetDir = elem
                    break

        if not targetDir:

            rootCandidate = OuterWorldInput.baseInstallDir
            if rootCandidate:
    
                for moreDir in self.searchGlobalConfigDirz:
                    elem = abspath(join(rootCandidate, moreDir) )
                    if isdir (elem):
                        targetDir = elem
                        break

        return targetDir

    def prepareConfigFilez(self):
        if not self.confFilez and not self.defFilez:
            generalTrace(['No config files to load.'])
            return False

        # first ones have higher priority and will redefine next configs
        self.confFilez.sort()
        generalTrace(self.confFilez)
        generalTrace(self.defFilez)
        return True

# ---------------------------------------------------------

# Copyright (c) 2013-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------
