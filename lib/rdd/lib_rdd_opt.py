#!/usr/bin/env python2
#encoding=utf-8

# dealing with input from command line

# : Outer data

from lib_rdd_print import *
from lib_rdd_optint import *
from lib_rdd_helpint import *

from sys import argv, exit
from os import environ
from os.path import basename

# ---------------------------------------------------------

# reading opts from command line

class OuterWorldInput(object):

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # static consts
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # maximum args in command string
    argMaxCount = 1000
    # maximum length of single arg
    oneArgMaxLength = 500
    # variable to put on verbose debug
    varInnerDebug = 'RDD_INNER_DEBUG'

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # static vars
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # global base directory
    baseInstallDir = ''


    # outermost reading
    @staticmethod
    def readRawParams():

        # writing to global variable (!)
        debugRecord = environ.get( OuterWorldInput.varInnerDebug, None)
        if debugRecord:
            OutModes.innerDebug = 1

        lenArgv = len(argv)
        if lenArgv > OuterWorldInput.argMaxCount:
            timeToPanic ([ "Num of params exceeded max limit of %d." % OuterWorldInput.argMaxCount ])
        for arg in OuterWorldInput.paranoidCensorship(argv[1:]):
            yield arg

    @staticmethod
    def paranoidCensorship(inpConv):

        for rl in inpConv:
            if len(rl) > OuterWorldInput.oneArgMaxLength:
                timeToPanic ([ "Length of single argument exceeded max limit of %d." % OuterWorldInput.oneArgMaxLength ])
            yield rl

# ---------------------------------------------------------

# interpretation for parameters

class CmdLineOptsMaster(object):

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # static consts
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    def __init__(self):
        # target vars, given in command line
        self.foundTargetDict = {}

    def safeDictKey(self, key):
        return key

    def parseCmdArgs(self):
        """ read argument list and:
            - save pairs somethinh=anything into inner dictionary.
            - give out the rest parameters.
        """
        self.foundTargetDict = {}
        for ownExecutable in OuterWorldInput.paranoidCensorship([ argv[0] ]):
            ownExecutable = basename(ownExecutable)

        for arg in OuterWorldInput.readRawParams():
            if arg == '--help':
                CmdLineInfo.printHelp(ownExecutable)
                exit ( 0 )
            if arg == '--version':
                CmdLineInfo.printVersion()
                exit ( 0 )

            keyCur = ''
            valCur = ''
            # make sure in correct format of key=value ;
            # first field (index of separator) should contain minimal 1 char
            if arg.find(CmdLineOptsInterface.separatorInOptVars) >= 1:

                # remember about possible several '=' in single arg
                keyCur, valCur = arg.split(CmdLineOptsInterface.separatorInOptVars, 1)
                self.foundTargetDict[self.safeDictKey(keyCur)] = valCur

            else:
                yield arg

    def getTargetDict(self):
        return self.foundTargetDict

    def readKeyPositions(self):
        """ Read params on fixed positions, defining the most frequent variables
            (entry point for properties and object's name)
        """
        
        varPositions = {}
        positions = []
        for singleOpt in self.parseCmdArgs():
            positions.append(singleOpt)

        lengthBoundary = len(positions)
        if lengthBoundary > 2:
            positions = positions[:3]
            lengthBoundary = 3

        # as object do, 1 - as, 2 - object, 3 - do
        if   lengthBoundary == 3:
            varPositions[CmdLineOptsInterface.shortParamForAs] = positions[0]
            varPositions[CmdLineOptsInterface.shortParamForObj] = positions[1]
            varPositions[CmdLineOptsInterface.shortParamForDo] = positions[2]

        elif lengthBoundary == 2:
            # action is regulated more often by inner map
            varPositions[CmdLineOptsInterface.shortParamForAs] = positions[0]
            varPositions[CmdLineOptsInterface.shortParamForObj] = positions[1]

        elif lengthBoundary == 1:
            # as (entry) or object ?

            # some sorcery could be possible at this place, for example, 
            # putting this in separate parameter and deciding later, entry 
            # or object should it mean. 
            # Desicion can be based on special rdd_tune setting or on defined 
            # vars: if def files have entry, this cmd param would be object, 
            # and vice versa.

            # now just take entry

            varPositions[CmdLineOptsInterface.shortParamForAs] = positions[0]

        return varPositions

    def interpretOuterOpts(self):
        varsFromOpts = {}
        varsFromKeyPositions = {}

        varsFromKeyPositions = self.readKeyPositions()

        varsFromOpts = self.getTargetDict()
        for singleVar in varsFromKeyPositions:
            if singleVar not in varsFromOpts:
                varsFromOpts[singleVar] = varsFromKeyPositions[singleVar]

        return varsFromOpts

# ---------------------------------------------------------

# Copyright (c) 2014-2020 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------
