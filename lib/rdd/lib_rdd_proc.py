#!/usr/bin/env python2
#encoding=utf-8

# general usage methods

from lib_rdd_print import timeToPanic, warningTrace, targetTrace

from os import access, X_OK, environ, getcwd
from os.path import isdir, isfile, join, abspath, isabs

def parseFile(fileName):
    fileKeeper = None
    try:
        fileKeeper = open(fileName)
    except Exception as e:
        warningTrace([ 'Cannot open file for reading:', str(e) ])
        return
    for line in fileKeeper.readlines():
        yield line
    fileKeeper.close()

def resolveBaseDir():
    """ Reference point for paths from configs
    """
    tryDir = environ.get('RDD_ROOT', None)
    if not tryDir:
        try:
            tryDir = getcwd()
            if not isdir(tryDir):
                timeToPanic(['Cannot get working dir.'])

        except Exception as e:
            timeToPanic(['Cannot get working dir.'])

    return tryDir

def whichExec(wannaExec):
    """ Simple 'which' substitute
    """
    pathVariable = environ.get('PATH')
    if pathVariable:
        for pathEntry in pathVariable.split(':'):
            
            pretender = join(pathEntry, wannaExec)
            if isfile(pretender) and access(pretender, X_OK):
                return abspath(pretender)
    return None

def resolveSingleCategoryPath(repo, key):
    """ Get first working path from repo with given key
    """
    localRoot = resolveBaseDir()
    for pathCand in repo.readPathStringVectValue(key):
        if not isabs(pathCand):
            pathCand = join(localRoot, pathCand)
        if isdir(pathCand) and access(pathCand, X_OK):
            return pathCand
    return None

def printVarz(dictWithVarz):
    """ Print out dict with variables
    """
    text = []
    sorted_keys = dictWithVarz.keys()
    sorted_keys.sort()
    for var in sorted_keys:
        text.append(var + '=' + dictWithVarz[var])
    targetTrace(text)
            

# ---------------------------------------------------------

# Copyright (c) 2013-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------

