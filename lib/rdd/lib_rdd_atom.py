#!/usr/bin/env python2
#encoding=utf-8

# data storage of single atom and handling within Python namespace

from os.path import expandvars, isfile, isabs
from lib_rdd_proc import parseFile, printVarz
from lib_rdd_print import targetTrace, timeToPanic
from lib_rdd_sanity import *

# -------------------------------------

class AtomKeeper(object):

    # for list elements in single string
    defListSeparator = ','
    # how to comment in value file
    defFileValComment = '#'
    # maximum length of string in value file
    defMaxString = 1023
    # maximum length of some name / id
    defMaxEssentialString = 127
    # chars allowed for path
    defPathChars = '~@.+- /'
    # constant to turn on\off something
    boolConfigYes = '1'
    
    def __init__(self, extDict=None):
        self.loadDict(extDict)

    def loadDict(self, extDict):
        # Big Dictionary of values
        if not extDict:
            raise Exception ('Tried to feed AtomKeeper with non-initialised value')
        self.treasureVarz = extDict

    def readValue(self, key):
        """ Main method to get value from storage by key
        """
        innerKey = self.correctKeyString(key)
        if innerKey in self.treasureVarz:
            return expandvars(self.treasureVarz[innerKey])
        return ''

    def readBoolValue(self, key):
        """ Is option "key" logically turned off\on
        """
        val = self.readValue(key)
        if val == AtomKeeper.boolConfigYes:
            return True
        # if no option explicitly given, it is turned off
        return False
        
    def correctKeyString(self, key):
        """ outer string to correct key string
        """
        if not self.checkKey(key):
            return None
        safeKey = key.lower()
        return safeKey

    def readVectValue(self, key):
        firstContact = self.readValue(key).strip()
        for retranslate in AtomKeeper.parseStringValue(firstContact):
            yield retranslate

    def readEssentialStringVectValue(self, key, morechars='', limitlength=0):
        limitLengthString = limitlength
        if limitLengthString <= 0:
            limitLengthString = AtomKeeper.defMaxEssentialString

        for desc in self.readVectValue(key):

            if len(desc) > limitLengthString:
                timeToPanic(['\'%s\': length limit of %s is exceeded.' % (desc[0:limitLengthString], limitLengthString)])

            if not areCharsEssential(desc, morechars):
                timeToPanic(['\'%s\': essential name must contain only alphabet letters, digits or \'_%s\'' %\
                        (desc,morechars)])
            yield desc

    # Do not try to delete '..' combinations from path.
    # Instead check file path before actual opening in according methods.
    def readPathStringVectValue(self, key, morechars=''):

        addMoreChars = AtomKeeper.defPathChars + morechars
        for path in self.readEssentialStringVectValue(key, \
                morechars=addMoreChars, limitlength=AtomKeeper.defMaxString):
            yield path

    @staticmethod
    def parseFileValue(fName):
        for pretender in parseFile(fName):
            pretender = pretender.strip()
            if pretender and not pretender.startswith(AtomKeeper.defFileValComment):
                yield pretender

    @staticmethod
    def parseStringValue(confString):
        for point in confString.split( AtomKeeper.defListSeparator ) :
            point = point.strip()
            if point:
                yield point

    def checkKey(self, wildString):
        """ is string safe to be a key in datastore
        """
        if len(wildString) > AtomKeeper.defMaxEssentialString:
            warningTrace([\
                    '\'%s\': length limit of %s for variable name is exceeded.' \
                    % (wildString[0:AtomKeeper.defMaxEssentialString], AtomKeeper.defMaxEssentialString),\
                    'Variable is not saved.'\
                    ])
            return False
        if not areCharsEssential(wildString):
            warningTrace([\
                    '\'%s\': variable name must contain only alphabet letters, digits or \'_\'' % wildString,\
                    'Variable is not saved.'\
                    ])
            return False 

        return True

    def checkValue(self, wildString):
        """ can the value be safely stored
        """
        if len(wildString) > AtomKeeper.defMaxString:
            warningTrace([\
                    'Content of variable \'%s\' exceeded the limit of %s.' % \
                    (wildString, AtomKeeper.defMaxString),
                    'Variable is not saved.'\
                    ])
            return False

        return True

    def saveValue(self, key, val):

        safeKey = self.checkKey(key)
        if safeKey:
            safeKey = self.checkValue(val)

        if safeKey:
            self.treasureVarz[key] = val
            return True

        return False

    def printData(self):
        printVarz(self.treasureVarz)

    def printAtomSeparator(self):
        text = [""]
        targetTrace(text)

# ---------------------------------------------------------

# Copyright (c) 2013-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------
