#!/usr/bin/env python2
#encoding=utf-8

# manage with linear lists of target atoms

from os import access, X_OK
from os.path import join, isdir, isfile, isabs, expandvars

from lib_rdd_proc import resolveBaseDir, resolveSingleCategoryPath
from lib_rdd_print import generalTrace
from lib_rdd_atom import *
from lib_rdd_atomplace import *
from lib_rdd_varint import *

# ---------------------------------------------------------

class ListMaster(object):

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # static consts
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # var in config with list data
    optListEntry = VarInterface.var_list_entry
    # var with path to search for lists
    optListPath = VarInterface.var_list_path

    limitMaxListFiles = 4096

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(ListMaster, cls).__new__(cls)
        return cls.instance

    def __init__(self):
        # list of objects to give out by generator
        self.readyQueue = []
        # storage with repo data
        self.repo = None
        # actual data for lists
        self.listEntry = ''
        # actual option to read the path to lists
        self.listPathConfig = ''
        # actual paths to lists
        self.listPath = []
        # index in array of generators for atoms
        self.curGenIndex = 0
        # name of first object, given as list
        self.beginList = ''

    # to load necroobject with all repo data
    def loadRepoData(self, repoExtern):
        self.repo = repoExtern

        self.initListEntries()
        self.resolveIterablePath()

    def initListEntries(self):
        """ init entry point for lists
        """
        self.listEntry = ListMaster.optListEntry
        self.listPathConfig = ListMaster.optListPath
        self.listPath = []

    def resolveIterablePath(self):
        localPath = resolveSingleCategoryPath(self.repo, self.listPathConfig)
        if localPath:
            self.listPath.append(localPath)

    def getNextConfigAtomDesc(self):
        self.initQueue()
        limitCounter = 0

        if self.beginList:
            atomDesc = self.constructAtom(self.beginList)
            if atomDesc:
                yield atomDesc

        entry = self.linearScheduleEmit()
        while entry:
            listFile = self.prepareListFromName(entry)
            if listFile:
                if limitCounter >= ListMaster.limitMaxListFiles:
                    timeToPanic(["""
Limit of %d on maximum open list files has been reached.
You are using too much list files OR
you have cycled listings (list contains previous lists).
""" % ListMaster.limitMaxListFiles ])
                limitCounter = limitCounter + 1

                self.linearScheduleLoad(listFile)
            else:
                atomDesc = self.constructAtom(entry)
                if atomDesc:
                    yield atomDesc
            entry = self.linearScheduleEmit()

    def initQueue(self):

        self.clearQueue()
        begin = self.repo.readValue(self.listEntry)
        self.linearScheduleLoad(begin)

        commaFound = False
        if begin.find(AtomKeeper.defListSeparator) >= 0:
            commaFound = True
        if isabs(begin) and not commaFound:
            self.beginList = begin

    def clearQueue(self):
        self.readyQueue = []
        self.curGenIndex = -1

    def linearScheduleEmit(self):
        """ Generate atoms in linear order from lists, probably nested
            Full conveyor in single iteration without recursion
        """
        entry = None
        if self.readyQueue[self.curGenIndex]:
            entry = self.readyQueue[self.curGenIndex].pop()
        while not entry and self.curGenIndex > 0:
            self.readyQueue.pop()
            self.curGenIndex = self.curGenIndex - 1
            if self.readyQueue[self.curGenIndex]:
                entry = self.readyQueue[self.curGenIndex].pop()
        return entry

    def prepareListFromName(self, entry):
        if isabs(entry) and isfile(entry) :
            return entry
        for path in self.listPath:
            pretender = join (path, entry)
            if isabs(pretender) and isfile(pretender):
                return pretender
        return None

    def linearScheduleLoad(self, entry):
        self.readyQueue.append([])
        self.curGenIndex = self.curGenIndex + 1
        for linePoint in self.parseValue(entry):
            self.readyQueue[self.curGenIndex].append(linePoint)
        if self.readyQueue[self.curGenIndex]:
            self.readyQueue[self.curGenIndex].reverse()

    def constructAtom(self, name):
        """ construct object of atom from string with it's name
        """
        atomContainer = AtomPlaceDescriptor(name)
        if atomContainer.loadRepo(self.repo):
            generalTrace( [ 'Constructing atom with name %s and path %s' % (atomContainer.getId(), atomContainer.getDir()) ] )
            return atomContainer
        return None

    def parseValue(self, anyData):

        if isabs(anyData) and isfile(anyData):
            for point in AtomKeeper.parseFileValue( anyData ):
                point =  expandvars(point)
                yield point
        else:
            for point in AtomKeeper.parseStringValue( anyData ):
                point =  expandvars(point)
                yield point

# ---------------------------------------------------------

# Copyright (c) 2013-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------

