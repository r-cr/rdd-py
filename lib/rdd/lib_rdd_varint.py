#!/usr/bin/env python2
#encoding=utf-8

# interface with inner variables of rdd data repository

class VarInterface(object):

    var_atom_id     = 'rdd_atom_id'
    var_atom_path   = 'rdd_atom_path'
    var_atom_dumpdir   = 'rdd_atom_dumpdir'

    var_prf_id      = 'rdd_prf_id'
    var_prf_entry   = 'rdd_prf_entry'
    var_prf_all     = 'rdd_prf_all'

    var_map_entry  = 'rdd_map_entry'
    var_map_group   = 'rdd_map_group'
    var_map_autopre = 'rdd_map_autopre'
    var_map_autopost = 'rdd_map_autopost'
    var_map_lang    = 'rdd_map_lang'
    var_map_libs    = 'rdd_map_libs'
    var_map_postmortem    = 'rdd_map_postmortem'

    prefix_var_lang = 'rdd_lang_'

    var_env_filter  = 'rdd_env_filter'

    var_list_entry  = 'rdd_list_entry'
    var_list_path   = 'rdd_list_path'

    var_log_stdout  = 'rdd_log_stdout'
    var_log_num_terminal = 'rdd_log_num_terminal'

    var_tune_phase_buffer = 'rdd_tune_phase_buffer_off'
    var_tune_atom_personal = 'rdd_tune_atom_personal'
    var_tune_stdout_redirect = 'rdd_tune_stdout_redirect'
    var_tune_stderr_redirect = 'rdd_tune_stderr_redirect'

 # ---------------------------------------------------------

# Copyright (c) 2014-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------
