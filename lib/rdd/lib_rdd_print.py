#!/usr/bin/env python2
#encoding=utf-8

# all purpose terminal output 

from __future__ import print_function
from sys import stderr, exit

class OutModes:
    
    innerDebug = 0
    generalDebug = 1
    mortalDebug = 1

    targetOutput = 1

class ErrorMessages:

    generalError = 1
    generalExit  = 2

class MessageMarkers:

    errorMarker = "Error: "
    warnMarker = "Warning: "
    debugMarker = "Debug: "

def plainTrace(traceTextMessage, prefix=""):
    """ Trace anything
    """
    for line in traceTextMessage:
        print (prefix + line, file=stderr)

def plainOutput(outerText):
    """ Print anything
    """
    for line in outerText:
        print (line)

def generalTrace(traceTextMessage):
    """ For variables and actions of rcr itself
    """
    if OutModes.innerDebug:
        plainTrace(traceTextMessage, MessageMarkers.debugMarker)

def timeToPanic(dieTextMessage, code=ErrorMessages.generalError):
    """ Emergency exit
    """
    if OutModes.mortalDebug:
        plainTrace(dieTextMessage, MessageMarkers.errorMarker)
    exit( code )

def shutDown(exitTextMessage, code=ErrorMessages.generalExit):
    """ Planned exit
    """
    if OutModes.innerDebug:
        plainTrace(exitTextMessage)
    exit( code )

def warningTrace(warningTextMessage):
    """ Inner checks
    """
    if OutModes.generalDebug:
        plainTrace(warningTextMessage, MessageMarkers.warnMarker)

def targetTrace(targetText):
    """ Working output of target modules
    """
    if OutModes.targetOutput:
        plainOutput(targetText)

def targetStringTrace(targetString):
    """ Working output of target modules, single string
    """
    if OutModes.targetOutput:
        print(targetString, end="")

# ---------------------------------------------------------

# Copyright (c) 2015-2017 Random Crew.
#
# Available under ISC License.

# ---------------------------------------------------------
