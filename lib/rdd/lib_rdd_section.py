#!/usr/bin/env python2
#encoding=utf-8

# reader of files in format of Python's ConfigParser

import ConfigParser
from copy import deepcopy

from lib_rdd_print import *
from lib_rdd_varint import *

# ---------------------------------------------------------

class SectionLoad(object):

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # static consts
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # options below contain values of another sections we need to read further

    optEntryAdditional = VarInterface.var_prf_id
    optEntryMain = VarInterface.var_prf_entry

    # separator for several values in single opt argument

    optWordSeparator = ','

    # config section, always added
    optSectionDefault = 'default'

    limitMaxSections = 65536

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(SectionLoad, cls).__new__(cls)
        return cls.instance

    def __init__(self):

        # what to begin the journey with
        self.sectzRoot = []

        # temporary fields

        # 2-dimensional list to gather sections from configs in the right order
        self.unreadCurStage = []
        self.curStageIndex = 0

        # array of config parsers, each for group of configs with same priority
        self.parserArray = []
        # current parser
        self.configParser = None

    def constructSectzFromCommandLine(self, staticVarz):
        for entry in [ SectionLoad.optEntryAdditional, SectionLoad.optEntryMain ]:
            if entry in staticVarz:
                stringSectzToParse = staticVarz[entry]
                self.addRootSect(stringSectzToParse)
        self.addRootSect(SectionLoad.optSectionDefault)

    def addRootSect(self, stringSectz):
        localSectList = self.stringToRootList(stringSectz)
        for localSect in localSectList:
            if not (localSect in self.sectzRoot ):
                self.sectzRoot.insert(0, localSect)

    def stringToRootList(self, stringSectz):
        localListSectz = stringSectz.split(SectionLoad.optWordSeparator)
        localListSectz.reverse()
        for cand in localListSectz:
            cand = cand.strip()
            if cand:
                yield cand

    def stringToSectList(self, stringSectz):
        localListSectz = stringSectz.split(SectionLoad.optWordSeparator)
        for cand in localListSectz:
            cand = cand.strip()
            if cand:
                yield cand

    def loadFilezIntoRepo(self, filesToLoad):

        localParser = None

        if filesToLoad:
            localParser = ConfigParser.ConfigParser()
            try:
                localParser.read(filesToLoad)
            except Exception as e:
                text = [
                        'Cannot parse config file:',
                        str(e),
                        'Emergency exit.'
                       ]
                timeToPanic(text)

        return localParser

    def constructSectz(self):

        targetSectz = deepcopy(self.sectzRoot)
        if not self.parserArray:
            targetSectz.reverse()
            return targetSectz

        for parser in self.parserArray:

            self.configParser = parser
            self.curStageIndex = 0
            del self.unreadCurStage
            self.unreadCurStage = []
            self.unreadCurStage.append(targetSectz)
            targetSectz = self.constructLayerSectz()
            targetSectz.reverse()

        targetSectz.reverse()
        return targetSectz
    
    def constructLayerSectz(self):

        targetSectz = []
        currentSect = self.getSectCurStage()
        limitCounter = 0
        while (currentSect):
            if limitCounter >= SectionLoad.limitMaxSections:
                timeToPanic(["""
Limit of %d on maximum sections, used at the same time, has been reached.
Check the correctness of your configuration (rdd.conf.d/ directory).
""" % SectionLoad.limitMaxSections ])
            limitCounter = limitCounter + 1

            if currentSect not in targetSectz:
                targetSectz.append(currentSect)
                self.addNewSectionsFromCurrent(currentSect)
            currentSect = self.getSectCurStage()

        return targetSectz

    def getSectCurStage(self):
        currentSect = None
        if self.unreadCurStage[self.curStageIndex]:
            currentSect = self.unreadCurStage[self.curStageIndex].pop()
        else:
            while (not self.unreadCurStage[self.curStageIndex]) and (self.curStageIndex > 0):
                self.curStageIndex = self.curStageIndex - 1
            if self.unreadCurStage[self.curStageIndex]:
                currentSect = self.unreadCurStage[self.curStageIndex].pop()
        return currentSect

    def addNewSectionsFromCurrent(self, section):
        for entry in [ SectionLoad.optEntryAdditional, SectionLoad.optEntryMain ]:
            if self.configParser.has_option(section, entry):
                stringSectzToParse = self.configParser.get(section, entry)
                localSectList = self.stringToSectList(stringSectzToParse)
                self.addLineOfCurrentStage(localSectList)

    def addLineOfCurrentStage(self, sectLine):
        self.unreadCurStage.append([])
        self.curStageIndex = self.curStageIndex + 1
        for sect in sectLine:
            self.unreadCurStage[self.curStageIndex].append(sect)

    def loadSingleParser(self, arrayOfConfigz):

        linearListOfFilesToLoad = []
        for listOfFiles in arrayOfConfigz:
            linearListOfFilesToLoad.extend(listOfFiles)
        parser = self.loadFilezIntoRepo(linearListOfFilesToLoad)
        self.parserArray.append(parser)

    def loadParsers(self, allConfigz):
        del self.parserArray
        self.parserArray = []

        lastConfig = None
        for focusedConfig in allConfigz:
            arrayOfConfigz = deepcopy(allConfigz)
            arrayOfConfigz.remove(focusedConfig)
            arrayOfConfigz.append(focusedConfig)
            self.loadSingleParser(arrayOfConfigz)

# interfaces

    def getLoadedSectz(self, commandLineVars, configz):

        self.constructSectzFromCommandLine(commandLineVars)
        self.loadParsers(configz)
        sectzTotal = self.constructSectz()

        return sectzTotal
