#!/usr/bin/env python2
#encoding=utf-8

# reader of data from files in INI format

import ConfigParser

from lib_rdd_opt import *
from lib_rdd_conf import *
from lib_rdd_section import *

from lib_rdd_print import *
from lib_rdd_varint import *

# ---------------------------------------------------------

class Loader(object):

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # static consts
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    optsToRemove = [ VarInterface.var_atom_id, VarInterface.var_log_stdout, \
             VarInterface.var_prf_id, VarInterface.var_atom_dumpdir ]
    # VarInterface.var_exit_status,
    optGeneratedAll = VarInterface.var_prf_all
    optGeneratedEntry = VarInterface.var_prf_entry

    # separator for words inside variables, exposed to outer scripts
    outerWordSeparator = ' '

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Loader, cls).__new__(cls)
        return cls.instance

    def __init__(self):

        self.optParser = CmdLineOptsMaster()
        self.configReader = ConfigMaster()
        self.configParser = ConfigParser.ConfigParser()

        self.sectionLoad = SectionLoad()

        # configuration files for ConfigParser
        self.configFiles = []
        # all sections, which were read from cmd and files
        self.configSections = []
        # options from command line, stored for cyclic data load
        self.optionsCommandLine = []

        # list of lists of corteges (three-dimensial crap) with target vars
        # we keep layered container to merge final dict of vars
        self.linezOfVarz = []
        # precious final data
        self.targetVarz = {}

    def dropVarz(self):
        del self.linezOfVarz
        self.linezOfVarz = []
        del self.targetVarz
        self.targetVarz = {}

    def loadVarLine(self, varListOfCorteges):
        self.linezOfVarz.append(varListOfCorteges)

    def loadFilezIntoRepo(self):

        flagExit = False
        linearListOfFilesToLoad = []
        del self.configParser
        self.configParser = ConfigParser.ConfigParser()

        for listOfFiles in self.configFiles:
            linearListOfFilesToLoad.extend(listOfFiles)

        if linearListOfFilesToLoad:
            self.configParser.read(linearListOfFilesToLoad)
            flagExit = True

        return flagExit

    def loadVarzFromCommandLine(self, varzCommandLine):
        self.dropVarz()
        self.loadVarLine(varzCommandLine.items())

    def loadVarzFromFilez(self):
        self.loadFilezIntoRepo()

        for section in self.configSections:
            if self.configParser.has_section(section):
                self.loadVarLine(self.configParser.items(section))

    def getConfigs(self, additional=[]):

        self.configReader.readAllConfigs()
        defz = self.configReader.getStaticConfigFilez()
        configz = self.configReader.getDynamicConfigFilez()
        resultConfigz = []
        if configz:
            resultConfigz.append(configz)
        if additional:
            resultConfigz.append(additional)
        if defz:
            resultConfigz.append(defz)
        return resultConfigz

    def mergeVarz(self):
        for lineOfVarz in self.linezOfVarz:
            for cortegeVarz in lineOfVarz:
                if not cortegeVarz[0] in self.targetVarz:
                    self.targetVarz [ cortegeVarz[0] ] = cortegeVarz[1]

    def loadGeneratedVarz(self):
        for var in Loader.optsToRemove:
            if var in self.targetVarz:
                del self.targetVarz[var]

        generatedValue = ''
        for section in self.configSections:
            generatedValue = section + Loader.outerWordSeparator + generatedValue
        generatedValue = generatedValue.strip(Loader.outerWordSeparator)
        self.targetVarz[Loader.optGeneratedAll] = generatedValue

# interfaces

    def getLoadedTargetData(self):

        self.optionsCommandLine = self.optParser.interpretOuterOpts()
        self.loadVarzFromCommandLine(self.optionsCommandLine)

        self.configFiles = self.getConfigs()
        self.configSections = self.sectionLoad.getLoadedSectz(self.optionsCommandLine, self.configFiles)
        self.loadVarzFromFilez()

        self.mergeVarz()
        self.loadGeneratedVarz()
        return self.targetVarz

    def getPersonalTargetData(self, personalConfigz ):

        self.dropVarz()

        self.loadVarzFromCommandLine(self.optionsCommandLine)

        self.configFiles = self.getConfigs(personalConfigz)
        self.configSections = self.sectionLoad.getLoadedSectz(self.optionsCommandLine, self.configFiles)
        self.loadVarzFromFilez()

        self.mergeVarz()
        self.loadGeneratedVarz()
        return self.targetVarz
