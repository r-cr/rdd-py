#!/usr/bin/env python2
#encoding=utf-8

# interfaces for printing info messages at cmd

class CmdLineInfo(object):

    @staticmethod
    def printHelp(executable):

        if executable == 'rdd': 
            print('''rdd: tool to launch script functions.

Usage:
rdd DATA [options]
rdd DATA OBJECTS [options]
rdd DATA OBJECTS ACTIONS [options]
rdd --help
rdd --version
Options: 
[option1=value1 option2=value2 ...]

DATA is identificator of config sections inside rdd.conf.d/ dir and rdd.def
    file, containing chosen options.
OBJECTS contain id of single or several objects, used for launch.
ACTIONS contain list of names of launched functions.

See rdd(1) and rdd-howto(7) man pages for detailed description.
''')

        if executable == 'datardd': 
            print('''datardd: tool to print data from rdd configs.

Usage:
datardd DATA [options]
datardd DATA OBJECT [options]
datardd --help
datardd --version
Options: 
[option1=value1 option2=value2 ...]

DATA is identificator of config sections inside rdd.conf.d/ dir and rdd.def
    file, containing chosen options.
OBJECT contain id of single object, used for launch. In case of list, given
in OBJECT, the first object from list is taken.

See datardd(1) and rdd-howto(7) man pages for detailed description.
''')

        if executable == 'dumprdd': 
            print('''dumprdd: tool to construct language script from rdd configs and print it to output.

Usage:
dumprdd DATA [options]
dumprdd DATA OBJECT [options]
dumprdd --help
dumprdd --version
Options: 
[option1=value1 option2=value2 ...]

DATA is identificator of config sections inside rdd.conf.d/ dir and rdd.def
    file, containing chosen options.
OBJECT contain id of single object, used for launch. In case of list, given
in OBJECT, the first object from list is taken.

See dumprdd(1) and rdd-howto(7) man pages for detailed description.
''')

        if executable == 'droprdd': 
            print('''droprdd: tool to construct language script from rdd configs and write it to file.
Resulting file names are printed to ouput.

Usage:
droprdd DATA [options]
droprdd DATA OBJECT [options]
droprdd --help
droprdd --version
Options: 
[option1=value1 option2=value2 ...]

DATA is identificator of config sections inside rdd.conf.d/ dir and rdd.def
    file, containing chosen options.
OBJECT contain id of single object, used for launch. In case of list, given
in OBJECT, the first object from list is taken.

See droprdd(1) and rdd-howto(7) man pages for detailed description.
''')

        if executable == 'lsrdd': 
            print('''lsrdd: tool to output list of rdd objects.

Usage:
lsrdd DATA [options]
lsrdd DATA OBJECTS [options]
lsrdd --help
lsrdd --version
Options: 
[option1=value1 option2=value2 ...]

DATA is identificator of config sections inside rdd.conf.d/ dir and rdd.def
    file, containing chosen options.
OBJECTS contain id of objects to output.

See lsrdd(1) and rdd-howto(7) man pages for detailed description.
''')

        if executable == 'rootrdd':
            print('''rootrdd: print the root of rdd working directory.

Usage:
rootrdd
rootrdd --help
rootrdd --version

See rootrdd(1) man page also, though there are not much more details.
''')

    @staticmethod
    def printVersion():
        print('1.2.1')
